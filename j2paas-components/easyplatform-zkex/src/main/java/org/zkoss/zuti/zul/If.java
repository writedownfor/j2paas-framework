/** If.java.

	Purpose:
		
	Description:
		
	History:
		12:48:20 PM Oct 22, 2014, Created by jumperchen

Copyright (C) 2014 Potix Corporation. All Rights Reserved.
 */
package org.zkoss.zuti.zul;

import java.util.HashMap;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.sys.BooleanPropertyAccess;
import org.zkoss.zk.ui.sys.PropertyAccess;

/**
 * The conditional execution of its body according to the value of the
 * <tt>test</tt> attribute
 * 
 * @author jumperchen
 * @since 8.0.0
 */
public class If extends TemplateBasedShadowElement {

	private static final long serialVersionUID = 2014102212482015L;

	private boolean _test;

	public void setTest(boolean test) {
		if (_test != test) {
			_test = test;
			_dirtyBinding = true;
		}
	}

	public boolean getTest() {
		return isTest();
	}

	public boolean isTest() {
		return _test;
	}

	protected boolean isEffective() {
		return getTest();
	}

	//--ComponentCtrl--//
	private static HashMap<String, PropertyAccess> _properties = new HashMap<String, PropertyAccess>(1);

	static {
		_properties.put("test", new BooleanPropertyAccess() {
			public void setValue(Component cmp, Boolean value) {
				((If) cmp).setTest(value);
			}

			public Boolean getValue(Component cmp) {
				return ((If) cmp).isTest();
			}
		});
	}

	public PropertyAccess getPropertyAccess(String prop) {
		PropertyAccess pa = _properties.get(prop);
		if (pa != null)
			return pa;
		return super.getPropertyAccess(prop);
	}
}
