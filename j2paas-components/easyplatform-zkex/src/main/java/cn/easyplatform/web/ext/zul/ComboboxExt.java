/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.zul;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.web.ext.*;
import org.zkoss.lang.Objects;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ComboboxExt extends Combobox implements ZkExt, Cacheable,
		Reloadable, Assignable, Queryable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 查询语句,结果column个数可以有1-3个,当1个的时候value和label共用，2个时分别表示value和label，3个时最后一个是图片
	 */
	private Object _query;

	/**
	 * 数据连接的资源id
	 */
	private String _dbId;

	/**
	 * 查询面板时使用
	 */
	private String _for;

	/**
	 * 查询面板时使用
	 */
	private String _type = "op";

	/**
	 * 在列表中是否要缓存本组件
	 */
	private boolean _cache;

	/**
	 * 在显示时是否要马上执行查询
	 */
	private boolean _immediate = true;

	/**
	 * 允许有一个自定义的选项
	 */
	private String _emptyLabel;

	/**
	 * 自定义选项的值
	 */
	private String _emptyValue = "";

	/**
	 * 是否要显示值，如果为true，value作为label，label作为desp
	 */
	private boolean _showValue;

	/**
	 * 过滤表达式
	 */
	private String _filter;
	/**
	 * 是否必需重新加载
	 */
	private boolean _force;

	@Override
	public boolean isForce() {
		return _force;
	}

	public void setForce(boolean force) {
		this._force = force;
	}

	public String getFilter() {
		return _filter;
	}

	public void setFilter(String filter) {
		this._filter = filter;
	}

	/**
	 * @return the showValue
	 */
	public boolean isShowValue() {
		return _showValue;
	}

	/**
	 * @param showValue
	 *            the showValue to set
	 */
	public void setShowValue(boolean showValue) {
		this._showValue = showValue;
	}

	/**
	 * @return the emptyLabel
	 */
	public String getEmptyLabel() {
		return _emptyLabel;
	}

	/**
	 * @param emptyLabel
	 *            the emptyLabel to set
	 */
	public void setEmptyLabel(String emptyLabel) {
		this._emptyLabel = emptyLabel;
	}

	/**
	 * @return the emptyValue
	 */
	public String getEmptyValue() {
		return _emptyValue;
	}

	/**
	 * @param emptyValue
	 *            the emptyValue to set
	 */
	public void setEmptyValue(String emptyValue) {
		this._emptyValue = emptyValue;
	}

	public boolean isImmediate() {
		return _immediate;
	}

	public void setImmediate(boolean immediate) {
		this._immediate = immediate;
	}

	public String getType() {
		return _type;
	}

	public void setType(String type) {
		this._type = type;
	}

	public Object getQuery() {
		return _query;
	}

	public void setQuery(Object query) {
		this._query = query;
	}

	public String getDbId() {
		return _dbId;
	}

	public void setDbId(String dbId) {
		this._dbId = dbId;
	}

	public String getFor() {
		return _for;
	}

	public void setFor(String _for) {
		this._for = _for;
	}

	public boolean isCache() {
		return _cache;
	}

	public void setCache(boolean cache) {
		this._cache = cache;
	}

	@Override
	public void reload() {
		Widget ext = (Widget) getAttribute("$proxy");
		ext.reload(this);
	}

	@Override
	public void setValue(Object value) {
		boolean ok = false;
		if (value != null) {
			for (Comboitem item : getItems()) {
				Object val = item.getValue();
				if (val == null)
					val = item.getLabel();
				if (Objects.equals(val.toString(), value.toString())) {
					setSelectedItem(item);
					ok = true;
					break;
				}
			}
			if (!ok && !Strings.isBlank(value.toString()))
				setValue(value.toString());
		}
		if (!ok)
			setSelectedIndex(-1);
		Object[] data = (Object[]) getAttribute("data");
		if (data != null) {
			Integer index = (Integer) getAttribute("index");
			if (index != null)
				data[index] = value;
		}
	}

	@Override
	public Object getRawValue() {
		Comboitem ci = getSelectedItem();
		if (ci != null)
			return ci.getValue() == null ? ci.getLabel() : ci.getValue();
		else if (!isReadonly())
			return getValue();
		else
			return null;
	}

	public Object getSelectedValue() {
		return getRawValue();
	}

	public String getLabel(Object value) {
		if (value != null) {
			for (Comboitem item : getItems()) {
				Object val = item.getValue();
				if (val == null)
					val = item.getLabel();
				if (Objects.equals(val, value)) {
					return item.getLabel();
				}
			}
			return value.toString();
		}
		return "";
	}

	public String getLabel() {
		Comboitem ci = getSelectedItem();
		if (ci != null)
			return ci.getLabel();
		return null;
	}

}
