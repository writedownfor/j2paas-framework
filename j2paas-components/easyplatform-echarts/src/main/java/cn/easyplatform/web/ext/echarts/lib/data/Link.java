/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.data;

import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;

import java.io.Serializable;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Link implements Serializable {
    private Object source;
    private Object target;
    private Object value;
    private ItemStyle lineStyle;
    private ItemStyle label;
    private Object symbol;
    private Object symbolSize;

    public Link(Object source, Object target) {
        this.source = source;
        this.target = target;
    }

    public Link(Object source, Object target, Object value) {
        this.source = source;
        this.target = target;
        this.value = value;
    }

    public Object symbol() {
        return symbol;
    }

    public Link symbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    public Object symbolSize() {
        return symbolSize;
    }

    public Link symbolSize(String symbolSize) {
        this.symbolSize = symbolSize;
        return this;
    }

    public Object label() {
        if (label == null)
            label = new ItemStyle();
        return label;
    }

    public Link label(ItemStyle label) {
        this.label = label;
        return this;
    }

    public Object lineStyle() {
        if (lineStyle == null)
            lineStyle = new ItemStyle();
        return lineStyle;
    }

    public Link lineStyle(ItemStyle lineStyle) {
        this.lineStyle = lineStyle;
        return this;
    }

    public Object value() {
        return value;
    }

    public Link value(String value) {
        this.value = value;
        return this;
    }

    public Object target() {
        return target;
    }

    public Link target(String target) {
        this.target = target;
        return this;
    }

    public Object source() {
        return source;
    }

    public Link source(String source) {
        this.source = source;
        return this;
    }

    public Object getSource() {
        return source;
    }

    public void setSource(Object source) {
        this.source = source;
    }

    public Object getTarget() {
        return target;
    }

    public void setTarget(Object target) {
        this.target = target;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public ItemStyle getLineStyle() {
        return lineStyle;
    }

    public void setLineStyle(ItemStyle lineStyle) {
        this.lineStyle = lineStyle;
    }

    public ItemStyle getLabel() {
        return label;
    }

    public void setLabel(ItemStyle label) {
        this.label = label;
    }

    public Object getSymbol() {
        return symbol;
    }

    public void setSymbol(Object symbol) {
        this.symbol = symbol;
    }

    public Object getSymbolSize() {
        return symbolSize;
    }

    public void setSymbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
    }
}
