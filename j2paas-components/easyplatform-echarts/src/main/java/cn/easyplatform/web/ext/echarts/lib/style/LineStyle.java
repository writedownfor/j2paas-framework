/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.style;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class LineStyle extends ShadowStyle {

    private static final long serialVersionUID = 4765717693423256102L;

    /**
     * 线条样式，可选为：'solid' | 'dotted' | 'dashed'
     */
    private String type;
    /**
     * 线宽
     */
    private Integer width;

    /**
     * 获取type值
     */
    public String type() {
        return this.type;
    }

    /**
     * 设置type值
     *
     * @param type
     */
    public LineStyle type(String type) {
        this.type = type;
        return this;
    }

    /**
     * 获取width值
     */
    public Integer width() {
        return this.width;
    }

    /**
     * 设置width值
     *
     * @param width
     */
    public LineStyle width(Integer width) {
        this.width = width;
        return this;
    }

    /**
     * 获取type值
     */
    public String getType() {
        return type;
    }

    /**
     * 设置type值
     *
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 获取width值
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * 设置width值
     *
     * @param width
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

}
