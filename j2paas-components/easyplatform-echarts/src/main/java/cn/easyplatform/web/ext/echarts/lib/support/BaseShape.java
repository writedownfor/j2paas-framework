/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.support;

public class BaseShape extends Point {
    /**
     * 标题背景颜色，默认透明
     */
    private String backgroundColor;
    /**
     * 标题边框颜色
     */
    private String borderColor;
    /**
     * borderWidth
     */
    private Integer borderWidth;
    /**
     * 圆角半径，单位px，支持传入数组分别指定 4 个圆角半径
     */
    private Object borderRadius;
    /**
     * 图形阴影的模糊大小。该属性配合 shadowColor,shadowOffsetX, shadowOffsetY 一起设置图形的阴影效果
     */
    private Integer shadowBlur;
    /**
     * 阴影颜色
     */
    private String shadowColor;
    /**
     * 阴影水平方向上的偏移距离
     */
    private Integer shadowOffsetX;
    /**
     * 阴影垂直方向上的偏移距离
     */
    private Integer shadowOffsetY;


    public Object borderRadius() {
        return this.borderRadius;
    }

    public BaseShape borderRadius(Object borderRadius) {
        this.borderRadius = borderRadius;
        return this;
    }

    public String backgroundColor() {
        return this.backgroundColor;
    }

    public BaseShape backgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
        return this;
    }

    public String borderColor() {
        return this.borderColor;
    }

    public BaseShape borderColor(String borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public Integer borderWidth() {
        return this.borderWidth;
    }

    public BaseShape borderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
        return this;
    }

    public Integer shadowBlur() {
        return this.shadowBlur;
    }

    public BaseShape shadowBlur(Integer shadowBlur) {
        this.shadowBlur = shadowBlur;
        return this;
    }

    public String shadowColor() {
        return this.shadowColor;
    }

    public BaseShape shadowColor(String shadowColor) {
        this.shadowColor = shadowColor;
        return this;
    }

    public Integer shadowOffsetX() {
        return this.shadowOffsetX;
    }

    public BaseShape shadowOffsetX(Integer shadowOffsetX) {
        this.shadowOffsetX = shadowOffsetX;
        return this;
    }

    public Integer shadowOffsetY() {
        return this.shadowOffsetY;
    }

    public BaseShape shadowOffsetY(Integer shadowOffsetY) {
        this.shadowOffsetY = shadowOffsetY;
        return this;
    }
}
