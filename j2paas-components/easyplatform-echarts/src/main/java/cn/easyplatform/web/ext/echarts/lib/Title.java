/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib;

import cn.easyplatform.web.ext.echarts.lib.style.TextStyle;
import cn.easyplatform.web.ext.echarts.lib.support.BaseShape;
import cn.easyplatform.web.ext.echarts.lib.type.Baseline;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Title extends BaseShape implements Serializable {
    /**
     * 是否显示
     */
    private Boolean show;
    /**
     * 主标题文本，'\n'指定换行
     */
    private String text;
    /**
     * 主标题文本超链接
     */
    private String link;
    /**
     * 指定窗口打开主标题超链接，支持'self' | 'blank'，不指定等同为'blank'（新窗口）
     */
    private String target;
    /**
     * 主标题文本样式（详见textStyle）
     */
    private TextStyle textStyle;
    /**
     * 副标题文本，'\n'指定换行
     */
    private String subtext;
    /**
     * 副标题文本超链接
     */
    private String sublink;
    /**
     * 指定窗口打开副标题超链接，支持'self' | 'blank'，不指定等同为'blank'（新窗口）
     */
    private String subtarget;
    /**
     * 默认值{color: '#aaa'}，副标题文本样式
     *
     * @see TextStyle
     */
    private TextStyle subtextStyle;
    /**
     * 水平对齐方式，默认根据x设置自动调整，可选为： left' | 'right' | 'center
     */
    private String textAlign;

    /**
     * 整体（包括 text 和 subtext）的垂直对齐。 可选值：'auto'、'top'、'bottom'、'middle'。
     */
    private Baseline textVerticalAlign;
    /**
     * 是否触发事件。
     */
    private Boolean triggerEvent;
    /**
     * 标题内边距，单位px，默认各方向内边距为5，接受数组分别设定上右下左边距，同css，见下图
     */
    private Object padding;
    /**
     * 主副标题纵向间隔，单位px，默认为10
     */
    private Integer itemGap;


    public Boolean triggerEvent() {
        return this.triggerEvent;
    }

    public Title triggerEvent(Boolean triggerEvent) {
        this.triggerEvent = triggerEvent;
        return this;
    }

    public Baseline textVerticalAlign() {
        return this.textVerticalAlign;
    }

    public Title textVerticalAlign(Baseline textVerticalAlign) {
        this.textVerticalAlign = textVerticalAlign;
        return this;
    }

    public Title textStyle(TextStyle textStyle) {
        this.textStyle = textStyle;
        return this;
    }

    public Title subtextStyle(TextStyle subtextStyle) {
        this.subtextStyle = subtextStyle;
        return this;
    }

    public String text() {
        return this.text;
    }

    public Title text(String text) {
        this.text = text;
        return this;
    }

    public String link() {
        return this.link;
    }

    public Title link(String link) {
        this.link = link;
        return this;
    }

    public String target() {
        return this.target;
    }

    public Title target(String target) {
        this.target = target;
        return this;
    }

    public String subtext() {
        return this.subtext;
    }

    public Title subtext(String subtext) {
        this.subtext = subtext;
        return this;
    }

    public String sublink() {
        return this.sublink;
    }

    public Title sublink(String sublink) {
        this.sublink = sublink;
        return this;
    }

    public String subtarget() {
        return this.subtarget;
    }

    public Title subtarget(String subtarget) {
        this.subtarget = subtarget;
        return this;
    }

    public String textAlign() {
        return this.textAlign;
    }

    public Title textAlign(String textAlign) {
        this.textAlign = textAlign;
        return this;
    }

    public TextStyle textStyle() {
        if (this.textStyle == null) {
            this.textStyle = new TextStyle();
        }
        return this.textStyle;
    }

    public TextStyle subtextStyle() {
        if (this.subtextStyle == null) {
            this.subtextStyle = new TextStyle();
        }
        return this.subtextStyle;
    }


    public Object padding() {
        return this.padding;
    }

    public Title padding(Object padding) {
        this.padding = padding;
        return this;
    }

    public Title padding(Object... padding) {
        if (padding != null && padding.length > 4) {
            throw new RuntimeException("padding属性最多可以接收4个参数!");
        }
        this.padding = padding;
        return this;
    }

    public Integer itemGap() {
        return this.itemGap;
    }

    public Title itemGap(Integer itemGap) {
        this.itemGap = itemGap;
        return this;
    }

    public Boolean show() {
        return this.show;
    }

    public Title show(Boolean show) {
        this.show = show;
        return this;
    }


    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public TextStyle getTextStyle() {
        return textStyle;
    }

    public void setTextStyle(TextStyle textStyle) {
        this.textStyle = textStyle;
    }

    public String getTextAlign() {
        return textAlign;
    }

    public void setTextAlign(String textAlign) {
        this.textAlign = textAlign;
    }

    public Baseline getTextVerticalAlign() {
        return textVerticalAlign;
    }

    public void setTextVerticalAlign(Baseline textVerticalAlign) {
        this.textVerticalAlign = textVerticalAlign;
    }

    public Boolean getTriggerEvent() {
        return triggerEvent;
    }

    public void setTriggerEvent(Boolean triggerEvent) {
        this.triggerEvent = triggerEvent;
    }

    public String getSubtext() {
        return subtext;
    }

    public void setSubtext(String subtext) {
        this.subtext = subtext;
    }

    public String getSublink() {
        return sublink;
    }

    public void setSublink(String sublink) {
        this.sublink = sublink;
    }

    public String getSubtarget() {
        return subtarget;
    }

    public void setSubtarget(String subtarget) {
        this.subtarget = subtarget;
    }

    public TextStyle getSubtextStyle() {
        return subtextStyle;
    }

    public void setSubtextStyle(TextStyle subtextStyle) {
        this.subtextStyle = subtextStyle;
    }

    public Object getPadding() {
        return padding;
    }

    public void setPadding(Object padding) {
        this.padding = padding;
    }

    public Integer getItemGap() {
        return itemGap;
    }

    public void setItemGap(Integer itemGap) {
        this.itemGap = itemGap;
    }
}
