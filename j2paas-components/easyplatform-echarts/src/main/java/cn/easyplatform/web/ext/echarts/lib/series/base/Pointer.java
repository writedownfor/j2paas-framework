/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series.base;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Pointer implements Serializable {
    private Boolean show;
    private Object length;
    private Object width;
    private String shadowColor;
    private Integer shadowBlur;

    public Boolean show() {
        return show;
    }

    public Pointer show(Boolean show) {
        this.show = show;
        return this;
    }

    public Object length() {
        return length;
    }

    public Pointer length(Object length) {
        this.length = length;
        return this;
    }

    public Object width() {
        return width;
    }

    public Pointer width(Object width) {
        this.width = width;
        return this;
    }

    public String shadowColor() {
        return this.shadowColor;
    }

    public Pointer shadowColor(String shadowColor) {
        this.shadowColor = shadowColor;
        return this;
    }

    public Integer shadowBlur() {
        return this.shadowBlur;
    }

    public Pointer shadowBlur(Integer shadowBlur) {
        this.shadowBlur = shadowBlur;
        return this;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public Object getLength() {
        return length;
    }

    public void setLength(Object length) {
        this.length = length;
    }

    public Object getWidth() {
        return width;
    }

    public void setWidth(Object width) {
        this.width = width;
    }

    public String getShadowColor() {
        return shadowColor;
    }

    public void setShadowColor(String shadowColor) {
        this.shadowColor = shadowColor;
    }

    public Integer getShadowBlur() {
        return shadowBlur;
    }

    public void setShadowBlur(Integer shadowBlur) {
        this.shadowBlur = shadowBlur;
    }
}
