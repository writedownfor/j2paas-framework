/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.threeD;

import cn.easyplatform.web.ext.echarts.lib.AxisPointer;
import cn.easyplatform.web.ext.echarts.lib.axis.AxisLabel;
import cn.easyplatform.web.ext.echarts.lib.axis.AxisTick;
import cn.easyplatform.web.ext.echarts.lib.axis.SplitArea;
import cn.easyplatform.web.ext.echarts.lib.axis.SplitLine;
import cn.easyplatform.web.ext.echarts.lib.support.Block;

import java.io.Serializable;

public class Grid3D extends Block implements Serializable {
    /**
     * 是否显示三维笛卡尔坐标系。
     */
    private boolean show;
    /**
     * 三维笛卡尔坐标系在三维场景中的宽度。配合 viewControl.distance 可以得到最合适的展示尺寸。
     */
    private int boxWidth;
    /**
     * 三维笛卡尔坐标系在三维场景中的高度。
     */
    private int boxHeight;
    /**
     * 三维笛卡尔坐标系在三维场景中的深度。
     */
    private int boxDepth;
    /**
     * 坐标轴轴线相关设置。
     */
    private AxisLine axisLine;
    /**
     * 坐标轴刻度标签的相关设置。
     */
    private AxisLabel axisLabel;
    /**
     * 坐标轴刻度相关设置。
     */
    private AxisTick axisTick;
    /**
     * 坐标轴轴线相关设置。
     */
    private SplitLine splitLine;
    /**
     * 坐标轴在 grid3D 的平面上的分隔区域。
     */
    private SplitArea splitArea;
    /**
     * 坐标轴指示线。
     */
    private AxisPointer axisPointer;
    /**
     * 环境贴图。支持纯色、渐变色、全景贴图的 url。默认为 'auto'，在配置有 light.ambientCubemap.texture 的时候会使用该纹理作为环境贴图。否则则不显示环境贴图。
     */
    private String environment;
    /**
     * 光照相关的设置。在 shading 为 'color' 的时候无效。
     *
     * 光照的设置会影响到组件以及组件所在坐标系上的所有图表。
     *
     * 合理的光照设置能够让整个场景的明暗变得更丰富，更有层次。
     */
    private Light light;
    /**
     * 后处理特效的相关配置。后处理特效可以为画面添加高光、景深、环境光遮蔽（SSAO）、调色等效果。可以让整个画面更富有质感。
     */
    private PostEffect postEffect;
    /**
     * 分帧超采样。在开启 postEffect 后，WebGL 默认的 MSAA 会无法使用，所以我们需要自己解决锯齿的问题。
     * 分帧超采样是用来解决锯齿问题的方法，它在画面静止后会持续分帧对一个像素多次抖动采样，从而达到抗锯齿的效果。
     * 而且在这个分帧采样的过程中，echarts-gl 也会对 postEffect 中一些需要采样保证效果的特效，例如 SSAO, 景深，以及阴影进行渐进增强。
     */
    private TemporalSuperSampling temporalSuperSampling;
    /**
     * viewControl用于鼠标的旋转，缩放等视角控制。
     */
    private ViewControl viewControl;

    public Boolean show() {
        return this.show;
    }
    public Grid3D show(Boolean show) {
        this.show = show;
        return this;
    }

    public Integer boxWidth() {
        return this.boxWidth;
    }
    public Grid3D boxWidth(Integer boxWidth) {
        this.boxWidth = boxWidth;
        return this;
    }

    public Integer boxHeight() {
        return this.boxHeight;
    }
    public Grid3D boxHeight(Integer boxHeight) {
        this.boxHeight = boxHeight;
        return this;
    }

    public Integer boxDepth() {
        return this.boxDepth;
    }
    public Grid3D boxDepth(Integer boxDepth) {
        this.boxDepth = boxDepth;
        return this;
    }

    public AxisLine axisLine() {
        return this.axisLine;
    }
    public Grid3D axisLine(AxisLine axisLine) {
        this.axisLine = axisLine;
        return this;
    }

    public AxisLabel axisLabel() {
        return this.axisLabel;
    }
    public Grid3D axisLabel(AxisLabel axisLabel) {
        this.axisLabel = axisLabel;
        return this;
    }

    public AxisTick axisTick() {
        return this.axisTick;
    }
    public Grid3D axisTick(AxisTick axisTick) {
        this.axisTick = axisTick;
        return this;
    }

    public SplitLine splitLine() {
        return this.splitLine;
    }
    public Grid3D splitLine(SplitLine splitLine) {
        this.splitLine = splitLine;
        return this;
    }

    public SplitArea splitArea() {
        return this.splitArea;
    }
    public Grid3D splitArea(SplitArea splitArea) {
        this.splitArea = splitArea;
        return this;
    }

    public AxisPointer axisPointer() {
        return this.axisPointer;
    }
    public Grid3D axisPointer(AxisPointer axisPointer) {
        this.axisPointer = axisPointer;
        return this;
    }

    public String environment() {
        return this.environment;
    }
    public Grid3D environment(String environment) {
        this.environment = environment;
        return this;
    }

    public Light light() {
        return this.light;
    }
    public Grid3D light(Light light) {
        this.light = light;
        return this;
    }

    public PostEffect postEffect() {
        return this.postEffect;
    }
    public Grid3D postEffect(PostEffect postEffect) {
        this.postEffect = postEffect;
        return this;
    }

    public TemporalSuperSampling temporalSuperSampling() {
        return this.temporalSuperSampling;
    }
    public Grid3D temporalSuperSampling(TemporalSuperSampling temporalSuperSampling) {
        this.temporalSuperSampling = temporalSuperSampling;
        return this;
    }

    public ViewControl viewControl() {
        return this.viewControl;
    }
    public Grid3D viewControl(ViewControl viewControl) {
        this.viewControl = viewControl;
        return this;
    }

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public int getBoxWidth() {
        return boxWidth;
    }

    public void setBoxWidth(int boxWidth) {
        this.boxWidth = boxWidth;
    }

    public int getBoxHeight() {
        return boxHeight;
    }

    public void setBoxHeight(int boxHeight) {
        this.boxHeight = boxHeight;
    }

    public int getBoxDepth() {
        return boxDepth;
    }

    public void setBoxDepth(int boxDepth) {
        this.boxDepth = boxDepth;
    }

    public AxisLine getAxisLine() {
        return axisLine;
    }

    public void setAxisLine(AxisLine axisLine) {
        this.axisLine = axisLine;
    }

    public AxisLabel getAxisLabel() {
        return axisLabel;
    }

    public void setAxisLabel(AxisLabel axisLabel) {
        this.axisLabel = axisLabel;
    }

    public AxisTick getAxisTick() {
        return axisTick;
    }

    public void setAxisTick(AxisTick axisTick) {
        this.axisTick = axisTick;
    }

    public SplitLine getSplitLine() {
        return splitLine;
    }

    public void setSplitLine(SplitLine splitLine) {
        this.splitLine = splitLine;
    }

    public SplitArea getSplitArea() {
        return splitArea;
    }

    public void setSplitArea(SplitArea splitArea) {
        this.splitArea = splitArea;
    }

    public AxisPointer getAxisPointer() {
        return axisPointer;
    }

    public void setAxisPointer(AxisPointer axisPointer) {
        this.axisPointer = axisPointer;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public Light getLight() {
        return light;
    }

    public void setLight(Light light) {
        this.light = light;
    }

    public PostEffect getPostEffect() {
        return postEffect;
    }

    public void setPostEffect(PostEffect postEffect) {
        this.postEffect = postEffect;
    }

    public TemporalSuperSampling getTemporalSuperSampling() {
        return temporalSuperSampling;
    }

    public void setTemporalSuperSampling(TemporalSuperSampling temporalSuperSampling) {
        this.temporalSuperSampling = temporalSuperSampling;
    }

    public ViewControl getViewControl() {
        return viewControl;
    }

    public void setViewControl(ViewControl viewControl) {
        this.viewControl = viewControl;
    }
}
