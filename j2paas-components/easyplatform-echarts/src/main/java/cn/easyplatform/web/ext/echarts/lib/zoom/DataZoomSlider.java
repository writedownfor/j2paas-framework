/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.zoom;

import cn.easyplatform.web.ext.echarts.lib.style.AreaStyle;
import cn.easyplatform.web.ext.echarts.lib.style.HandleStyle;
import cn.easyplatform.web.ext.echarts.lib.style.LineStyle;
import cn.easyplatform.web.ext.echarts.lib.style.TextStyle;
import cn.easyplatform.web.ext.echarts.lib.type.DataZoomType;

import java.io.Serializable;

/**
 * 数据区域缩放。与toolbox.feature.dataZoom同步，仅对直角坐标系图表有效
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DataZoomSlider extends DataZoom {
    /**
     * 组件的背景颜色
     */
    private String backgroundColor;
    /**
     * 默认#ccc，数据缩略背景颜色
     */
    private DataBackgroundColor dataBackgroundColor;
    /**
     * 默认值rgba(144,197,237,0.2)，选择区域填充颜色
     */
    private String fillerColor;
    /**
     * 边框颜色 default: '#ddd'
     */
    private String borderColor;
    /**
     * 手柄的 icon 形状，支持路径字符串
     */
    private String handleIcon;
    /**
     * 控制手柄的尺寸，可以是像素大小，也可以是相对于 dataZoom 组件宽度的百分比，默认跟 dataZoom 宽度相同
     */
    private Integer handleSize;
    /**
     * 手柄的样式配置
     */
    private HandleStyle handleStyle;

    /**
     * 显示label的小数精度。默认根据数据自动决定
     */
    private String labelPrecision;
    /**
     * 显示的label的格式化器
     */
    private String labelFormatter;
    /**
     * 缩放变化是否显示定位详情
     */
    private Boolean showDetail;
    /**
     * 是否在 dataZoom-silder 组件中显示数据阴影。数据阴影可以简单得反应数据走势
     */
    private String showDataShadow;
    /**
     * 缩放变化是否实时显示，建议性能较低的浏览器或数据量巨大时不启动实时效果
     */
    private Boolean realtime;
    /**
     * 文字样式
     */
    private TextStyle textStyle;
    /**
     * 一级层叠控制
     */
    private Integer zlevel;
    /**
     * 二级层叠控制
     */
    private Integer z;
    /**
     * treemap 组件离容器左侧的距离
     */
    private Object left;
    /**
     * treemap 组件离容器上侧的距离
     */
    private Object top;
    /**
     * treemap 组件离容器右侧的距离
     */
    private Object right;
    /**
     * treemap 组件离容器下侧的距离
     */
    private Object bottom;

    public DataZoomSlider() {
        this.type(DataZoomType.slider.name());
    }

    public Object left() {
        return this.left;
    }

    public DataZoomSlider left(Object left) {
        this.left = left;
        return this;
    }

    public Object top() {
        return this.top;
    }

    public DataZoomSlider top(Object top) {
        this.top = top;
        return this;
    }

    public Object right() {
        return this.right;
    }

    public DataZoomSlider right(Object right) {
        this.right = right;
        return this;
    }

    public Object bottom() {
        return this.bottom;
    }

    public DataZoomSlider bottom(Object bottom) {
        this.bottom = bottom;
        return this;
    }

    public DataZoomSlider zlevel(Integer zlevel) {
        this.zlevel = zlevel;
        return this;
    }

    public Integer zlevel() {
        return this.zlevel;
    }

    public DataZoomSlider z(Integer z) {
        this.z = z;
        return this;
    }

    public Integer z() {
        return this.z;
    }

    public HandleStyle handleStyle() {
        if (handleStyle == null)
            handleStyle = new HandleStyle();
        return handleStyle;
    }

    public DataZoomSlider handleStyle(HandleStyle handleStyle) {
        this.handleStyle = handleStyle;
        return this;
    }

    public String handleIcon() {
        return handleIcon;
    }

    public DataZoomSlider handleIcon(String handleIcon) {
        this.handleIcon = handleIcon;
        return this;
    }

    public String borderColor() {
        return borderColor;
    }

    public DataZoomSlider borderColor(String borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public String backgroundColor() {
        return backgroundColor;
    }

    public DataZoomSlider backgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
        return this;
    }

    public String labelPrecision() {
        return this.labelPrecision;
    }

    public DataZoomSlider labelPrecision(String labelPrecision) {
        this.labelPrecision = labelPrecision;
        return this;
    }

    public String labelFormatter() {
        return this.labelFormatter;
    }

    public DataZoomSlider labelFormatter(String labelFormatter) {
        this.labelFormatter = labelFormatter;
        return this;
    }

    public String showDataShadow() {
        return this.showDataShadow;
    }

    public DataZoomSlider showDataShadow(String showDataShadow) {
        this.showDataShadow = showDataShadow;
        return this;
    }

    public TextStyle textStyle() {
        if (this.textStyle == null) {
            this.textStyle = new TextStyle();
        }
        return this.textStyle;
    }

    public DataZoomSlider textStyle(TextStyle textStyle) {
        this.textStyle = textStyle;
        return this;
    }

    public String getLabelPrecision() {
        return labelPrecision;
    }

    public void setLabelPrecision(String labelPrecision) {
        this.labelPrecision = labelPrecision;
    }

    public String getLabelFormatter() {
        return labelFormatter;
    }

    public void setLabelFormatter(String labelFormatter) {
        this.labelFormatter = labelFormatter;
    }

    public String getShowDataShadow() {
        return showDataShadow;
    }

    public void setShowDataShadow(String showDataShadow) {
        this.showDataShadow = showDataShadow;
    }

    public TextStyle getTextStyle() {
        return textStyle;
    }

    public void setTextStyle(TextStyle textStyle) {
        this.textStyle = textStyle;
    }

    public Integer handleSize() {
        return this.handleSize;
    }

    public DataZoomSlider handleSize(Integer handleSize) {
        this.handleSize = handleSize;
        return this;
    }

    public DataBackgroundColor dataBackgroundColor() {
        return this.dataBackgroundColor;
    }

    public DataZoomSlider dataBackgroundColor(DataBackgroundColor dataBackgroundColor) {
        this.dataBackgroundColor = dataBackgroundColor;
        return this;
    }

    public String fillerColor() {
        return this.fillerColor;
    }

    public DataZoomSlider fillerColor(String fillerColor) {
        this.fillerColor = fillerColor;
        return this;
    }

    public Boolean realtime() {
        return this.realtime;
    }

    public DataZoomSlider realtime(Boolean realtime) {
        this.realtime = realtime;
        return this;
    }

    public Boolean showDetail() {
        return this.showDetail;
    }

    public DataZoomSlider showDetail(Boolean showDetail) {
        this.showDetail = showDetail;
        return this;
    }

    public DataBackgroundColor getDataBackgroundColor() {
        return dataBackgroundColor;
    }

    public void setDataBackgroundColor(DataBackgroundColor dataBackgroundColor) {
        this.dataBackgroundColor = dataBackgroundColor;
    }

    public String getFillerColor() {
        return fillerColor;
    }

    public void setFillerColor(String fillerColor) {
        this.fillerColor = fillerColor;
    }

    public Boolean getRealtime() {
        return realtime;
    }

    public void setRealtime(Boolean realtime) {
        this.realtime = realtime;
    }

    public Boolean getShowDetail() {
        return showDetail;
    }

    public void setShowDetail(Boolean showDetail) {
        this.showDetail = showDetail;
    }

    public Integer getHandleSize() {
        return handleSize;
    }

    public void setHandleSize(Integer handleSize) {
        this.handleSize = handleSize;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public String getHandleIcon() {
        return handleIcon;
    }

    public void setHandleIcon(String handleIcon) {
        this.handleIcon = handleIcon;
    }

    public HandleStyle getHandleStyle() {
        return handleStyle;
    }

    public void setHandleStyle(HandleStyle handleStyle) {
        this.handleStyle = handleStyle;
    }

    public Integer getZlevel() {
        return zlevel;
    }

    public void setZlevel(Integer zlevel) {
        this.zlevel = zlevel;
    }

    public Integer getZ() {
        return z;
    }

    public void setZ(Integer z) {
        this.z = z;
    }

    public Object getLeft() {
        return left;
    }

    public void setLeft(Object left) {
        this.left = left;
    }

    public Object getTop() {
        return top;
    }

    public void setTop(Object top) {
        this.top = top;
    }

    public Object getRight() {
        return right;
    }

    public void setRight(Object right) {
        this.right = right;
    }

    public Object getBottom() {
        return bottom;
    }

    public void setBottom(Object bottom) {
        this.bottom = bottom;
    }

    public static class DataBackgroundColor implements Serializable {
        private LineStyle lineStyle;

        private AreaStyle areaStyle;

        public LineStyle lineStyle() {
            return this.lineStyle;
        }

        public DataBackgroundColor lineStyle(LineStyle lineStyle) {
            this.lineStyle = lineStyle;
            return this;
        }

        public AreaStyle areaStyle() {
            return this.areaStyle;
        }

        public DataBackgroundColor areaStyle(AreaStyle areaStyle) {
            this.areaStyle = areaStyle;
            return this;
        }
    }
}
