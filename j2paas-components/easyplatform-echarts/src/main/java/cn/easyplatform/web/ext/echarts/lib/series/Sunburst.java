/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;

import cn.easyplatform.web.ext.echarts.lib.style.Emphasis;
import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;
import cn.easyplatform.web.ext.echarts.lib.style.LabelStyle;
import cn.easyplatform.web.ext.echarts.lib.support.Animation;

public class Sunburst extends Animation {

    private String type = "sunburst";

    private String name;
    /**
     * 一级层叠控制
     */
    private Integer zlevel;
    /**
     * 二级层叠控制
     */
    private Integer z;
    /**
     * 旭日图的中心（圆心）坐标，数组的第一项是横坐标，第二项是纵坐标。
     */
    private Object[] center;
    /**
     * 旭日图的半径
     */
    private Object radius;

    private LabelStyle label;

    private ItemStyle itemStyle;

    private Emphasis emphasis;
    /**
     * 鼠标悬停后相关扇形块的配置项
     */
    private Highlight highlight;
    /**
     * 鼠标悬停后相关扇形块的配置项
     */
    private Highlight downplay;

    private Object[] levels;

    public Highlight downplay() {
        if (downplay == null)
            downplay = new Highlight();
        return this.downplay;
    }

    public Sunburst downplay(Highlight downplay) {
        this.downplay = downplay;
        return this;
    }

    public Highlight highlight() {
        if (highlight == null)
            highlight = new Highlight();
        return this.highlight;
    }

    public Sunburst highlight(Highlight highlight) {
        this.highlight = highlight;
        return this;
    }

    public Emphasis emphasis() {
        if (emphasis == null)
            emphasis = new Emphasis();
        return this.emphasis;
    }

    public Sunburst emphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
        return this;
    }

    public ItemStyle itemStyle() {
        if (itemStyle == null)
            itemStyle = new ItemStyle();
        return this.itemStyle;
    }

    public Sunburst label(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
        return this;
    }

    public LabelStyle label() {
        if (label == null)
            label = new LabelStyle();
        return this.label;
    }

    public Sunburst label(LabelStyle label) {
        this.label = label;
        return this;
    }

    public Object[] levels() {
        return levels;
    }

    public Sunburst levels(Object[] levels) {
        this.levels = levels;
        return this;
    }

    public Object radius() {
        return radius;
    }

    public Sunburst radius(Object radius) {
        this.radius = radius;
        return this;
    }

    public Object[] center() {
        return center;
    }

    public Sunburst center(Object[] center) {
        this.center = center;
        return this;
    }

    public String name() {
        return name;
    }

    public Sunburst name(String name) {
        this.name = name;
        return this;
    }

    public Sunburst zlevel(Integer zlevel) {
        this.zlevel = zlevel;
        return this;
    }

    public Integer zlevel() {
        return this.zlevel;
    }

    public Integer z() {
        return this.z;
    }

    public Sunburst z(Integer z) {
        this.z = z;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getZlevel() {
        return zlevel;
    }

    public void setZlevel(Integer zlevel) {
        this.zlevel = zlevel;
    }

    public Integer getZ() {
        return z;
    }

    public void setZ(Integer z) {
        this.z = z;
    }

    public Object[] getCenter() {
        return center;
    }

    public void setCenter(Object[] center) {
        this.center = center;
    }

    public Object getRadius() {
        return radius;
    }

    public void setRadius(Object radius) {
        this.radius = radius;
    }

    public LabelStyle getLabel() {
        return label;
    }

    public void setLabel(LabelStyle label) {
        this.label = label;
    }

    public ItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }

    public Emphasis getEmphasis() {
        return emphasis;
    }

    public void setEmphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
    }

    public Highlight getHighlight() {
        return highlight;
    }

    public void setHighlight(Highlight highlight) {
        this.highlight = highlight;
    }

    public Highlight getDownplay() {
        return downplay;
    }

    public void setDownplay(Highlight downplay) {
        this.downplay = downplay;
    }

    public Object[] getLevels() {
        return levels;
    }

    public void setLevels(Object[] levels) {
        this.levels = levels;
    }

    public static class Highlight {
        private LabelStyle label;
        private ItemStyle itemStyle;

        public ItemStyle itemStyle() {
            if (itemStyle == null)
                itemStyle = new ItemStyle();
            return this.itemStyle;
        }

        public Highlight label(ItemStyle itemStyle) {
            this.itemStyle = itemStyle;
            return this;
        }

        public LabelStyle label() {
            if (label == null)
                label = new LabelStyle();
            return this.label;
        }

        public Highlight label(LabelStyle label) {
            this.label = label;
            return this;
        }

        public LabelStyle getLabel() {
            return label;
        }

        public void setLabel(LabelStyle label) {
            this.label = label;
        }

        public ItemStyle getItemStyle() {
            return itemStyle;
        }

        public void setItemStyle(ItemStyle itemStyle) {
            this.itemStyle = itemStyle;
        }
    }
}
