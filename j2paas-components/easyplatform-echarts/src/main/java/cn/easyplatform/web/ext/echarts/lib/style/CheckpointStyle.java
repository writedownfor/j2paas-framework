/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.style;

import java.io.Serializable;

/**
 * 时间轴当前点，该类只在Timeline中使用
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CheckpointStyle implements Serializable {

    private static final long serialVersionUID = -5003403667974720869L;

    /**
     * 当前点symbol，默认随轴上的symbol
     */
    private Object symbol;
    /**
     * 当前点symbol大小，默认随轴上symbol大小
     */
    private Object symbolSize;
    /**
     * timeline标记的旋转角度。注意在 markLine 中当 symbol 为 'arrow' 时会忽略 symbolRotate 强制设置为切线的角度
     */
    private Integer symbolRotate;
    /**
     * timeline标记相对于原本位置的偏移。默认情况下，标记会居中置放在数据对应的位置，但是如果 symbol 是自定义的矢量路径或者图片，就有可能不希望 symbol 居中。这时候可以使用该配置项配置 symbol 相对于原本居中的偏移，可以是绝对的像素值，也可以是相对的百分比
     */
    private Object[] symbolOffset;
    /**
     * 当前点symbol颜色，默认为随当前点颜色，可指定具体颜色，如无则为'#1e90ff'
     */
    private String color;
    /**
     * 当前点symbol边线颜色
     */
    private String borderColor;
    /**
     * 当前点symbol边线宽度
     */
    private Object borderWidth;
    /**
     * timeline组件中『当前项』（checkpoint）在 timeline 播放切换中的移动，是否有动画
     */
    private Boolean animation;
    /**
     * timeline组件中『当前项』（checkpoint）的动画时长
     */
    private Integer animationDuration;
    /**
     * timeline组件中『当前项』（checkpoint）的动画的缓动效果
     */
    private String animationEasing;

    public Integer symbolRotate() {
        return symbolRotate;
    }

    public CheckpointStyle symbolRotate(Integer symbolRotate) {
        this.symbolRotate = symbolRotate;
        return this;
    }

    public Object[] symbolOffset() {
        return symbolOffset;
    }

    public CheckpointStyle symbolOffset(Object... symbolOffset) {
        this.symbolOffset = symbolOffset;
        return this;
    }

    public Boolean animation() {
        return animation;
    }

    public CheckpointStyle animation(Boolean animation) {
        this.animation = animation;
        return this;
    }

    public String animationEasing() {
        return animationEasing;
    }

    public CheckpointStyle animationEasing(String animationEasing) {
        this.animationEasing = animationEasing;
        return this;
    }

    public Integer animationDuration() {
        return animationDuration;
    }

    public CheckpointStyle animationDuration(Integer animationDuration) {
        this.animationDuration = animationDuration;
        return this;
    }

    /**
     * 获取symbol值
     */
    public Object symbol() {
        return this.symbolRotate;
    }

    /**
     * 设置symbol值
     *
     * @param symbol
     */
    public CheckpointStyle symbol(Object symbol) {
        this.symbol = symbol;
        return this;
    }

    /**
     * 获取symbolSize值
     */
    public Object symbolSize() {
        return this.symbolSize;
    }

    /**
     * 设置symbolSize值
     *
     * @param symbolSize
     */
    public CheckpointStyle symbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
        return this;
    }

    /**
     * 获取color值
     */
    public String color() {
        return this.color;
    }

    /**
     * 设置color值
     *
     * @param color
     */
    public CheckpointStyle color(String color) {
        this.color = color;
        return this;
    }

    /**
     * 获取borderColor值
     */
    public String borderColor() {
        return this.borderColor;
    }

    /**
     * 设置borderColor值
     *
     * @param borderColor
     */
    public CheckpointStyle borderColor(String borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    /**
     * 获取borderWidth值
     */
    public Object borderWidth() {
        return this.borderWidth;
    }

    /**
     * 设置borderWidth值
     *
     * @param borderWidth
     */
    public CheckpointStyle borderWidth(Object borderWidth) {
        this.borderWidth = borderWidth;
        return this;
    }

    /**
     * 获取symbol值
     */
    public Object getSymbol() {
        return symbol;
    }

    /**
     * 设置symbol值
     *
     * @param symbol
     */
    public void setSymbol(Object symbol) {
        this.symbol = symbol;
    }

    /**
     * 获取symbolSize值
     */
    public Object getSymbolSize() {
        return symbolSize;
    }

    /**
     * 设置symbolSize值
     *
     * @param symbolSize
     */
    public void setSymbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
    }

    /**
     * 获取color值
     */
    public String getColor() {
        return color;
    }

    /**
     * 设置color值
     *
     * @param color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * 获取borderColor值
     */
    public String getBorderColor() {
        return borderColor;
    }

    /**
     * 设置borderColor值
     *
     * @param borderColor
     */
    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    /**
     * 获取borderWidth值
     */
    public Object getBorderWidth() {
        return borderWidth;
    }

    /**
     * 设置borderWidth值
     *
     * @param borderWidth
     */
    public void setBorderWidth(Object borderWidth) {
        this.borderWidth = borderWidth;
    }
}
