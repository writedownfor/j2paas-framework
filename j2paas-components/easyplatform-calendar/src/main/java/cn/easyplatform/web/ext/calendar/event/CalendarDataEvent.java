/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.calendar.event;

import java.io.Serializable;
import java.util.Date;
import java.util.TimeZone;

import cn.easyplatform.web.ext.calendar.api.CalendarEvent;
import cn.easyplatform.web.ext.calendar.api.CalendarModel;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CalendarDataEvent implements Serializable {
	private static final long serialVersionUID = 20090317164042L;
	/** Identifies one or more changes in the lists contents. */
	public static final int CONTENTS_CHANGED = 0;
    /** Identifies the addition of one or more contiguous items to the list. */    
	public static final int INTERVAL_ADDED = 1;
    /** Identifies the removal of one or more contiguous items from the list. */   
	public static final int INTERVAL_REMOVED = 2;

	private final CalendarModel _model;
	private final int _type;
	private final Date _begin, _end;
	private final TimeZone _timezone;
	private final CalendarEvent _e;
	
	/** Contructor.
	 *@deprecated As of release 2.0-RC, replaced with {@link #CalendarDataEvent(CalendarModel model, int type, CalendarEvent e, TimeZone timezone)}
	 * @param type one of {@link #CONTENTS_CHANGED},
	 * {@link #INTERVAL_ADDED}, {@link #INTERVAL_REMOVED}.
	 */
	public CalendarDataEvent(CalendarModel model, int type, Date begin, Date end, TimeZone timezone) {
		if (model == null)
			throw new IllegalArgumentException();
		_model = model;
		_type = type;
		_begin = begin;
		_end = end;
		_timezone = timezone;
		_e = null;
	}
	
	/** Contructor.
	 * @since 1.1.1_50
	 * @param type one of {@link #CONTENTS_CHANGED},
	 * {@link #INTERVAL_ADDED}, {@link #INTERVAL_REMOVED}.
	 */
	public CalendarDataEvent(CalendarModel model, int type, CalendarEvent e, TimeZone timezone) {
		if (model == null)
			throw new IllegalArgumentException();
		_model = model;
		_type = type;
		_begin = e != null ? e.getBeginDate() : null;
		_end = e != null ? e.getEndDate() : null;
		_e = e;
		_timezone = timezone;
	}
	/** Returns the calendar model that fires this event.
	 */
	public CalendarModel getModel() {
		return _model;
	}
	/** Returns the event type. One of {@link #CONTENTS_CHANGED},
	 * {@link #INTERVAL_ADDED}, {@link #INTERVAL_REMOVED}.
	 */
	public int getType() {
		return _type;
	}
	/** Returns the begin date of the change range.
	 */
	public Date getBeginDate() {
		return _begin;
	}
	/** Returns the end date of the change range.
	 */
	public Date getEndDate() {
		return _end;
	}
	/**
	 * Return the time zone of the calendar
	 */
	public TimeZone getTimeZone() {
		return _timezone;
	}
	/**
	 * @since 1.1.1_50
	 * Return the CalendarEvent of the calendar
	 */
	public CalendarEvent getCalendarEvent() {
		return _e;
	}
	
	//Object//
	public String toString() {
		return "[CalendarDataEvent type=" + _type +", begin="+_begin+", end="+_end+']';
	}
}
