/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.demo.table.expert;

import cn.easyplatform.entities.beans.table.TableField;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class OracleTableExpert extends AbstractTableExpert {

	@Override
	protected String evalFieldType(TableField mf) {
		switch (mf.getType()) {
		case BOOLEAN:
			return "NUMBER(1)";
		case CLOB:
			return "CLOB";
		case VARCHAR:
			return "VARCHAR2(" + mf.getLength() + ")";
		case LONG:
			return "INTEGER";
		case INT:
			// 用户自定义了宽度
			if (mf.getLength() > 0)
				return "NUMBER(" + mf.getLength() + ")";
			return "NUMBER(8)";
		case NUMERIC:
			// 用户自定义了精度
			if (mf.getLength() > 0 && mf.getDecimal() > 0) {
				return "NUMBER(" + mf.getLength() + "," + mf.getDecimal()
						+ ")";
			}else
			// 用默认精度
				return "NUMBER(15,2)";
		case TIME:
		case DATETIME:
		case DATE:
			return "DATE";
		default:
			return super.evalFieldType(mf);
		}
	}
}
