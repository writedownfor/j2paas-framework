/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.component;

import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.entities.helper.EventLogic;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.addon.SaveListRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.support.scripting.CompliableScriptEngine;
import cn.easyplatform.support.scripting.ScriptEngineFactory;
import cn.easyplatform.type.CrossRowVo;
import cn.easyplatform.type.FieldVo;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.RuntimeUtils;

import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SaveListCmd extends AbstractCommand<SaveListRequestMessage> {

    /**
     * @param req
     */
    public SaveListCmd(SaveListRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        RecordContext rc = cc.getWorkflowContext().getRecord();
        List<CrossRowVo> list = req.getBody();
        EventLogic el = RuntimeUtils.castTo(cc, rc, req.getLogic());
        if (el != null) {
            CompliableScriptEngine engine = ScriptEngineFactory.createCompilableEngine(cc, el.getContent());
            rc = rc.clone();
            try {
                for (CrossRowVo crv : list) {
                    for (FieldVo fv : crv.getData())
                        rc.setVariable(RuntimeUtils.castTo(fv));
                    engine.eval(cc.getWorkflowContext().getRecord(), rc);
                }
            } finally {
                engine.destroy();
                engine = null;
            }
        }
        return new SimpleResponseMessage();
    }
}
