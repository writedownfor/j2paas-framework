/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.runtime;

import cn.easyplatform.EasyPlatformWithLabelKeyException;
import cn.easyplatform.engine.runtime.batch.BatchTask;
import cn.easyplatform.engine.runtime.bpm.BpmTask;
import cn.easyplatform.engine.runtime.datalist.DatalistTask;
import cn.easyplatform.engine.runtime.jasper.ReportTask;
import cn.easyplatform.engine.runtime.page.PageTask;
import cn.easyplatform.type.EntityType;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public final class RuntimeTaskFactory {

	private RuntimeTaskFactory() {
	}

	public static RuntimeTask createRuntime(String type) {
		if (type.equals(EntityType.PAGE.getName()))
			return new PageTask();
		if (type.equals(EntityType.BATCH.getName()))
			return new BatchTask();
		if (type.equals(EntityType.BPM.getName()))
			return new BpmTask();
		if (type.equals(EntityType.DATALIST.getName()))
			return new DatalistTask();
		if (type.equals(EntityType.REPORT.getName()))
			return new ReportTask();
		throw new EasyPlatformWithLabelKeyException("task.type.not.support", "",
				type);
	}
}
