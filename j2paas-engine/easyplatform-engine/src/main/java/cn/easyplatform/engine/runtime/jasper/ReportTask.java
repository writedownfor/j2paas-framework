/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.runtime.jasper;

import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.contexts.WorkflowContext;
import cn.easyplatform.engine.runtime.RuntimeTask;
import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.entities.beans.report.JasperReportBean;
import cn.easyplatform.entities.helper.EventLogic;
import cn.easyplatform.i18n.I18N;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.response.PageResponseMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.PageVo;
import cn.easyplatform.type.Constants;
import cn.easyplatform.type.EntityType;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.type.SubType;
import cn.easyplatform.util.MessageUtils;
import cn.easyplatform.util.RuntimeUtils;

import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ReportTask implements RuntimeTask {

    @Override
    public IResponseMessage<?> doTask(CommandContext cc, BaseEntity bean) {
        WorkflowContext ctx = cc.getWorkflowContext();
        EventLogic el = RuntimeUtils.castTo(cc, ctx.getRecord(),
                ctx.getParameter("818"));
        if (el != null) {
            // 如果有上一个或者是从已有的触发的功能，在这里可以实行变量的声明以及mapping的处理
            // $xxx=@xxx
            // $表示当前的栏位或变量，@表示上一个功能的变量或栏位
            //
            ctx.getRecord().setParameter("808", Constants.ON_INIT);
            RecordContext[] rcs = ctx.getMappingRecord();
            String result = RuntimeUtils.eval(cc, el, rcs);
            if (!result.equals("0000"))
                return MessageUtils.byErrorCode(cc, ctx.getRecord(),
                        ctx.getId(), result);
        }
        if (bean.getSubType().equals(SubType.JASPER.getName())) {
            JasperReportBean rb = (JasperReportBean) bean;
            ctx.setParameter("830", EntityType.REPORT.getName());
            ctx.setParameter("831", rb.getId()).setParameter("832",
                    rb.getName());
            ctx.setParameter("833", "");
            return new PageResponseMessage(createPage(ctx, rb));
        } else if (bean.getSubType().equals(SubType.PIVOT.getName())) {

        }
        return new SimpleResponseMessage("e011", I18N.getLabel(
                "report.type.invalid", bean.getId(), bean.getSubType()));
    }

    @Override
    public IResponseMessage<?> doNext(CommandContext cc,
                                      Map<String, Object> data) {
        return null;
    }

    private PageVo createPage(WorkflowContext ctx, JasperReportBean jr) {
        PageVo pv = new PageVo();
        pv.setId(ctx.getId());
        String title = ctx.getParameterAsString("811");
        if (title != null) {
            if (title.charAt(0) == '$')
                title = (String) ctx.getRecord().getValue(title.substring(1));
        } else
            title = "";
        pv.setTitile(title);
        pv.setHeight(0);
        pv.setWidth(0);
        pv.setImage(ctx.getParameterAsString("813"));
        pv.setOpenModel(ctx.getParameterAsInt("805"));
        pv.setProcessCode(ctx.getParameterAsString("814"));
        pv.setVisible(ctx.getParameterAsBoolean("816"));
        StringBuilder sb = new StringBuilder();
        sb.append("<report entity=\"").append(jr.getId()).append("\" id=\"_")
                .append(jr.getId()).append("_\" hflex=\"1\" vflex=\"1\"/>");
        pv.setPage(sb.toString());
        return pv;
    }

    @Override
    public IResponseMessage<?> doPrev(CommandContext cc) {
        WorkflowContext ctx = cc.getWorkflowContext();
        BaseEntity bean = cc.getEntity(ctx.getParameterAsString("831"));
        if (bean.getSubType().equals(SubType.JASPER.getName()))
            return new PageResponseMessage(createPage(ctx,
                    (JasperReportBean) bean));
        else if (bean.getSubType().equals(SubType.PIVOT.getName())) {

        }
        return new SimpleResponseMessage("e011", I18N.getLabel(
                "report.type.invalid", bean.getId(), bean.getSubType()));
    }

}
