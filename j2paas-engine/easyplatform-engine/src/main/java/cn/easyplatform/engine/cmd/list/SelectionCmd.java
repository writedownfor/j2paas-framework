/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.list;

import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.contexts.WorkflowContext;
import cn.easyplatform.dos.Record;
import cn.easyplatform.engine.runtime.datalist.DataListUtils;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Lang;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.ListSelectionRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.datalist.ListSelectionVo;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;

import java.util.Iterator;
import java.util.List;


/**
 * 选中列表的记录
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SelectionCmd extends AbstractCommand<ListSelectionRequestMessage> {

	/**
	 * @param req
	 */
	public SelectionCmd(ListSelectionRequestMessage req) {
		super(req);
	}

	@Override
	public IResponseMessage<?> execute(CommandContext cc) {
		ListSelectionVo sv = req.getBody();
		WorkflowContext ctx = cc.getWorkflowContext();
		ListContext lc = ctx.getList(sv.getId());
		if (lc == null)
			return MessageUtils.dataListNotFound(sv.getId());
		List<RecordContext> rcs = lc.getRecords();
		if (rcs != null) {
			for (RecordContext rc : rcs) {
				Iterator<Object[]> itr = sv.getKeys().iterator();
				boolean find = false;
				while (itr.hasNext()) {
					Object[] key = itr.next();
					if (Lang.equals(rc.getKeyValues(), key)) {
						if (Strings.isBlank(ctx.getParameterAsString("833")))
							ctx.setData(rc.getData());
						itr.remove();
						find = true;
						break;
					}
				}
				rc.setParameter("853", find);
			}
		}
		if (!sv.getKeys().isEmpty()) {
			for (Object[] data : sv.getKeys()) {
				RecordContext rc = null;
				if (lc.isCustom()) {
					rc = lc.createRecord(data);
				} else {
					Record record = DataListUtils.getRecord(cc, lc, data);
					rc = lc.createRecord(data, record);
					rc.setParameter("814", 'R');
					rc.setParameter("815", Boolean.FALSE);
				}
				lc.appendRecord(rc);
				rc.setParameter("853", true);
				if (Strings.isBlank(ctx.getParameterAsString("833")))
					ctx.setData(rc.getData());
			}
		}
		return new SimpleResponseMessage();
	}

	@Override
	public String getName() {
		return "list.Selection";
	}
}
