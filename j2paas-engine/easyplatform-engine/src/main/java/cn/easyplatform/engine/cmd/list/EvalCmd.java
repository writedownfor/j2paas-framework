/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.list;

import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.contexts.WorkflowContext;
import cn.easyplatform.engine.runtime.datalist.DataListUtils;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.ListEvalRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.datalist.ListEvalVo;
import cn.easyplatform.messages.vos.datalist.ListUpdatableRowVo;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;
import cn.easyplatform.util.RuntimeUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class EvalCmd extends AbstractCommand<ListEvalRequestMessage> {

	/**
	 * @param req
	 */
	public EvalCmd(ListEvalRequestMessage req) {
		super(req);
	}

	@Override
	public IResponseMessage<?> execute(CommandContext cc) {
		WorkflowContext ctx = cc.getWorkflowContext();
		ListEvalVo vo = req.getBody();
		ListContext target = ctx.getList(vo.getTargetId());
		if (target == null)
			return MessageUtils.dataListNotFound(vo.getTargetId());
		if (!Strings.isBlank(vo.getSourceId())) {
			ListContext lc = ctx.getList(vo.getSourceId());
			if (lc == null)
				return MessageUtils.dataListNotFound(vo.getSourceId());
			List<ListUpdatableRowVo> tdata = new ArrayList<ListUpdatableRowVo>();
			List<ListUpdatableRowVo> sdata = new ArrayList<ListUpdatableRowVo>();
			for (int i = 0; i < vo.getSourceKeys().size(); i++) {
				Object[] sourceKey = vo.getSourceKeys().get(i);
				RecordContext source = lc.getRecord(sourceKey);
				if (source == null) {
					source = target.createRecord(DataListUtils.getRecord(cc,
							lc, sourceKey));
					lc.appendRecord(source, false);
				}
				for (Object[] key : vo.getTargetKeys()) {
					RecordContext rc = target.getRecord(key);
					if (rc == null) {
						rc = target.createRecord(DataListUtils.getRecord(cc,
								target, key));
						rc.setParameter("814", "U");
						rc.setParameter("815", true);
						target.appendRecord(rc, false);
					}
					String code = RuntimeUtils.eval(cc, vo.getLogic(), source,
							rc);
					if (!code.equals("0000"))
						return MessageUtils.byErrorCode(cc, rc, ctx.getId(),
								code);
					if (i == vo.getSourceKeys().size() - 1)// 最后一个循环才执行
						tdata.add(new ListUpdatableRowVo(rc.getKeyValues(),
								DataListUtils.wrapRow(cc, target, rc), false,
								rc.getParameterAsBoolean("853")));
				}
				sdata.add(new ListUpdatableRowVo(source.getKeyValues(),
						DataListUtils.wrapRow(cc, lc, source), false,
						source.getParameterAsBoolean("853")));
			}
			return new SimpleResponseMessage(new Object[] { tdata, sdata });
		} else {
			List<ListUpdatableRowVo> data = new ArrayList<ListUpdatableRowVo>();
			for (Object[] key : vo.getTargetKeys()) {
				RecordContext rc = target.getRecord(key);
				if (rc == null) {
					rc = target.createRecord(DataListUtils.getRecord(cc,
							target, key));
					rc.setParameter("814", "U");
					rc.setParameter("815", true);
					target.appendRecord(rc, false);
				}
				String code = RuntimeUtils.eval(cc, vo.getLogic(),
						ctx.getRecord(), rc);
				if (!code.equals("0000"))
					return MessageUtils.byErrorCode(cc, rc, ctx.getId(), code);
				data.add(new ListUpdatableRowVo(rc.getKeyValues(),
						DataListUtils.wrapRow(cc, target, rc), false, rc
								.getParameterAsBoolean("853")));
			}
			return new SimpleResponseMessage(data);
		}
	}

	@Override
	public String getName() {
		return "list.Eval";
	}
}
