/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.service;

import cn.easyplatform.EngineService;
import cn.easyplatform.engine.cmd.api.*;
import cn.easyplatform.engine.cmd.api.ApiCmd;
import cn.easyplatform.engine.cmd.api.PollCmd;
import cn.easyplatform.engine.cmd.api.PagingCmd;
import cn.easyplatform.messages.request.ApiInitRequestMessage;
import cn.easyplatform.messages.request.ApiRequestMessage;
import cn.easyplatform.messages.request.BeginRequestMessage;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.request.im.PagingRequestMessage;
import cn.easyplatform.spi.service.ApiService;
import cn.easyplatform.type.IResponseMessage;

/**
 * @Author: <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @Description:
 * @Since: 2.0.0 <br/>
 * @Date: Created in 2019/10/29 10:59
 * @Modified By:
 */
public class ApiServiceImpl extends EngineService implements ApiService {

    @Override
    public IResponseMessage<?> auth(ApiInitRequestMessage req) {
        return commandExecutor.execute(new AuthCmd(req));
    }

    @Override
    public IResponseMessage<?> call(SimpleRequestMessage req) {
        return commandExecutor.execute(new CallCmd(req));
    }

    @Override
    public IResponseMessage<?> api(ApiRequestMessage req) {
        return commandExecutor.execute(new ApiCmd(req));
    }

    @Override
    public IResponseMessage<?> anonymous(SimpleRequestMessage req) {
        return commandExecutor.execute(new AnonymousCmd(req));
    }

    @Override
    public IResponseMessage<?> quick(SimpleRequestMessage req) {
        return commandExecutor.execute(new QuickCmd(req));
    }

    @Override
    public IResponseMessage<?> task(BeginRequestMessage req) {
        return commandExecutor.execute(new TaskCmd(req));
    }

    @Override
    public IResponseMessage<?> token(SimpleRequestMessage req) {
        return commandExecutor.execute(new TokenCmd(req));
    }

    @Override
    public IResponseMessage<?> exit(SimpleRequestMessage req) {
        return commandExecutor.execute(new ExitCmd(req));
    }

    @Override
    public IResponseMessage<?> poll(SimpleRequestMessage req) {
        return commandExecutor.execute(new PollCmd(req));
    }

    @Override
    public IResponseMessage<?> paging(PagingRequestMessage req) {
        return commandExecutor.execute(new PagingCmd(req));
    }
}
