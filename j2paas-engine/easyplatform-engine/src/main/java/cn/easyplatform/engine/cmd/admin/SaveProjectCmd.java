/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.admin;

import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.admin.CustomVo;
import cn.easyplatform.type.IResponseMessage;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SaveProjectCmd extends AbstractCommand<SimpleRequestMessage> {

    /**
     * @param req
     */
    public SaveProjectCmd(SimpleRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        try {
            if (req.getBody() instanceof CustomVo) {//保存项目配置项
                cc.getProjectService().getConfig().save(cc, (CustomVo) req.getBody());
                return new SimpleResponseMessage(cc.getProjectService().getConfig().getOptions());
            }
            cc.getProjectService().save(req.getBody());
            return new SimpleResponseMessage();
        } catch (Exception e) {
            return new SimpleResponseMessage("e099", e.getMessage());
        }
    }

}
