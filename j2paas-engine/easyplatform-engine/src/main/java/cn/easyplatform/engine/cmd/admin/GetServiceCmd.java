/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.admin;

import cn.easyplatform.contexts.WorkflowContext;
import cn.easyplatform.entities.beans.ResourceBean;
import cn.easyplatform.entities.beans.project.ProjectBean;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.admin.ServiceRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.TaskRuntimeVo;
import cn.easyplatform.messages.vos.admin.ProjectVo;
import cn.easyplatform.messages.vos.admin.ResourceVo;
import cn.easyplatform.messages.vos.admin.ServiceVo;
import cn.easyplatform.services.IScheduleService;
import cn.easyplatform.spi.extension.ApplicationService;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.type.ServiceType;
import cn.easyplatform.util.EntityUtils;
import cn.easyplatform.util.RuntimeUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetServiceCmd extends AbstractCommand<ServiceRequestMessage> {

    /**
     * @param req
     */
    public GetServiceCmd(ServiceRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        ServiceVo vo = req.getBody();
        if (vo.getType() == ServiceType.PROJECT) {
            ProjectBean pb = cc.getProjectService().getEntity();
            ProjectVo pv = (ProjectVo) EntityUtils.entity2Vo(pb);
            pv.setState(cc.getProjectService().getState());
            pv.setRuntimeInfo(cc.getProjectService().getRuntimeInfo());
            return new SimpleResponseMessage(pv);
        } else if (vo.getType() == ServiceType.DATASOURCE) {
            ApplicationService s = null;
            if (cc.getProjectService() == null)
                s = cc.getEngineConfiguration().getService(vo.getId());
            else
                s = cc.getProjectService().getService(vo.getId());
            return new SimpleResponseMessage(s.getRuntimeInfo());
        } else if (vo.getType() == ServiceType.JOB) {
            ResourceBean rb = cc.getEntity(vo.getId());
            ResourceVo rv = (ResourceVo) EntityUtils.entity2Vo(rb);
            IScheduleService service = cc
                    .getEngineConfiguration().getScheduleService();
            rv.setRuntimeInfo(service.getRuntimeInfo(cc.getProjectService()
                    .getId(), vo.getId()));
            return new SimpleResponseMessage(rv);
        } else if (vo.getType() == ServiceType.USER) {//获取用户正在运行的功能信息
            List<WorkflowContext> works = cc.getWorkflowContexts(vo.getId());
            if (!works.isEmpty()) {
                List<TaskRuntimeVo> result = new ArrayList<>(works.size());
                for (WorkflowContext ctx : works)
                    result.add(RuntimeUtils.createRuntime(ctx, false));
                return new SimpleResponseMessage(result);
            }
        }
        return new SimpleResponseMessage();
    }

}
