/* Copyright 2013-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.snaker.engine.cfg;

import cn.easyplatform.bpm.access.EasyPlatformTransactionInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snaker.engine.Context;
import org.snaker.engine.SnakerEngine;
import org.snaker.engine.SnakerException;
import org.snaker.engine.access.transaction.TransactionInterceptor;
import org.snaker.engine.core.ServiceContext;
import org.snaker.engine.core.SnakerEngineImpl;

import javax.sql.DataSource;

/**
 * 只允许应用程序存在一个Configuration实例 初始化服务上下文，查找流程引擎实现类并初始化依赖的服务
 * 
 * @author yuqs
 * @since 1.0
 */
public class Configuration {
	/**
	 * 
	 */
	private static final Logger log = LoggerFactory
			.getLogger(Configuration.class);

	/**
	 * 根据服务查找实现类构造配置对象
	 * 
	 * @param context
	 *            上下文实现
	 */
	public Configuration(Context context) {	
		ServiceContext.setContext(context);
	}

	/**
	 * 构造SnakerEngine对象，用于api集成 通过SpringHelper调用
	 * 
	 * @return SnakerEngine
	 * @throws SnakerException
	 */
	public SnakerEngine buildSnakerEngine(DataSource ds) throws SnakerException {
		if (log.isInfoEnabled()) {
			log.info("SnakerEngine start......");
		}
		TransactionInterceptor interceptor = new EasyPlatformTransactionInterceptor();
		SnakerEngine configEngine = interceptor
				.getProxy(SnakerEngineImpl.class);
		return configEngine.configure(ds, interceptor);
	}
}
