/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.spi.service;

import cn.easyplatform.messages.request.*;
import cn.easyplatform.type.IResponseMessage;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface ListService {

    IResponseMessage<?> doInit(ListInitRequestMessage req);

    IResponseMessage<?> doPaging(ListPagingRequestMessage req);

    IResponseMessage<?> doQuery(ListQueryRequestMessage req);

    IResponseMessage<?> doRefresh(ListRefreshRequestMessage req);

    IResponseMessage<?> doSave(ListSaveRequestMessage req);

    IResponseMessage<?> doDelete(ListDeleteRequestMessage req);

    IResponseMessage<?> doCreate(ListCreateRequestMessage req);

    IResponseMessage<?> doSelect(ListSelectRequestMessage req);

    IResponseMessage<?> doClose(SimpleRequestMessage req);

    IResponseMessage<?> doOpen(ListOpenRequestMessage req);

    IResponseMessage<?> doReload(ListReloadRequestMessage req);

    IResponseMessage<?> doSort(ListSortRequestMessage req);

    IResponseMessage<?> doSelection(ListSelectionRequestMessage req);

    IResponseMessage<?> doClear(SimpleTextRequestMessage req);

    IResponseMessage<?> doReset(ListResetRequestMessage req);

    IResponseMessage<?> doDrop(DropRequestMessage req);

    IResponseMessage<?> evalList(ListEvalRequestMessage req);

    IResponseMessage<?> setPageSize(ListPagingRequestMessage req);

    IResponseMessage<?> doLoad(SimpleRequestMessage req);
}
