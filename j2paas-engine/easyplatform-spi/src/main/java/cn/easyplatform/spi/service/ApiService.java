/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.spi.service;

import cn.easyplatform.messages.request.*;
import cn.easyplatform.messages.request.im.PagingRequestMessage;
import cn.easyplatform.type.IRequestMessage;
import cn.easyplatform.type.IResponseMessage;

/**
 * @Author: <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @Description:
 * @Since: 2.0.0 <br/>
 * @Date: Created in 2019/10/29 10:53
 * @Modified By:
 */
public interface ApiService {

    /**
     * 验证用户，返回jwt主体
     *
     * @param req
     * @return
     */
    IResponseMessage<?> auth(ApiInitRequestMessage req);

    /**
     * 匿名调用api
     *
     * @param req
     * @return
     */
    IResponseMessage<?> call(SimpleRequestMessage req);

    /**
     * api调用
     *
     * @param req
     * @return
     */
    IResponseMessage<?> api(ApiRequestMessage req);

    /**
     * 匿名调用功能
     *
     * @param req
     * @return
     */
    IResponseMessage<?> anonymous(SimpleRequestMessage req);

    /**
     * 快速登陆，小程调用webview
     *
     * @param req
     * @return
     */
    IResponseMessage<?> quick(SimpleRequestMessage req);

    /**
     * 运行功能
     *
     * @param req
     * @return
     */
    IResponseMessage<?> task(BeginRequestMessage req);

    /**
     * 重新获取token
     *
     * @param req
     * @return
     */
    IResponseMessage<?> token(SimpleRequestMessage req);

    /**
     * 退出
     *
     * @param req
     * @return
     */
    IResponseMessage<?> exit(SimpleRequestMessage req);

    /**
     * 获取消息记录
     *
     * @param req
     * @return
     */
    IResponseMessage<?> poll(SimpleRequestMessage req);

    /**
     * 消息分页
     *
     * @param req
     * @return
     */
    IResponseMessage<?> paging(PagingRequestMessage req);
}
