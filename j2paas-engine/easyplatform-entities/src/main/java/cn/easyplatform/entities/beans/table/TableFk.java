/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.table;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
@XmlAccessorType(XmlAccessType.NONE)
public class TableFk implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7976120342768910834L;

	@XmlAttribute(required = true)
	private String name;

	@XmlAttribute
	private String action;

	@XmlAttribute(required = true)
	private String references;

	@XmlElement
	private String sourceFields;
	
	@XmlElement
	private String toFields;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action
	 *            the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return the references
	 */
	public String getReferences() {
		return references;
	}

	/**
	 * @param references
	 *            the references to set
	 */
	public void setReferences(String references) {
		this.references = references;
	}

	/**
	 * @return
	 */
	public String getSourceFields() {
		return sourceFields;
	}

	/**
	 * @param sourceFields
	 */
	public void setSourceFields(String sourceFields) {
		this.sourceFields = sourceFields;
	}

	/**
	 * @return
	 */
	public String getToFields() {
		return toFields;
	}

	/**
	 * @param toFields
	 */
	public void setToFields(String toFields) {
		this.toFields = toFields;
	}

	@Override
	public String toString() {
		return "foreign-key:" + name;
	}

}
