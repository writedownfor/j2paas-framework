/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.dao;

import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.entities.beans.ResourceBean;

import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface EntityDao {

    /**
     * @return
     */
    List<EntityInfo> getAllEntity(String table);

    /**
     * @return
     */
    List<EntityInfo> getUpdatableEntity(String table);

    /**
     * @return
     */
    <T extends BaseEntity> List<T> getEntities(String table, String type, String subType);

    /**
     * @return
     */
    List<String[]> getEntities(String sql, Page page, Object... params);

    /**
     * @return
     */
    List<ResourceBean> getCaches(String table, boolean sessionScope);

    /**
     * @param entityId
     * @return
     */
    EntityInfo getEntity(String table, String entityId);

    /**
     *
     */
    void updateEntity(String table);

    /**
     * @return
     */
    List<EntityInfo> getModels();

    /**
     * @return
     */
    List<EntityInfo> getModels(String projectId, String type);

    /**
     * @param id
     * @return
     */
    EntityInfo getModel(String id);

    /**
     * 更新状态
     *
     * @param id
     * @param status
     */
    void setModelStatus(String id, String status);

    /**
     * @param model
     */
    void updateModel(EntityInfo model);

    /**
     * @param entity
     */
    void updateEntity(String table, EntityInfo entity);

    /**
     * 自动生成项目id(四位，第1个只能是字母，以小写a开始，到z，其它3位数字和字母递增)
     *
     * @return
     */
    char[] generateId();

    /**
     * @param statement
     * @param parameter
     * @return
     */
    Object selectObject(String statement, Object... parameter);

    /**
     * @param statement
     * @param parameter
     * @return
     */
    Map<String, Object> selectOne(String statement, Object... parameter);

    /**
     * @param statement
     * @param parameter
     * @return
     */
    List<Map<String, Object>> selectList(String statement, Object... parameter);

    /**
     * @param statement
     * @param parameter
     * @return
     */
    int update(String statement, Object... parameter);

    /**
     * @param statement
     * @param parameter
     * @return
     */
    long insert(String statement, Object... parameter);
}
