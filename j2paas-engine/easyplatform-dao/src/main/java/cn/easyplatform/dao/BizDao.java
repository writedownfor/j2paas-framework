/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.dao;

import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.dos.LogDo;
import cn.easyplatform.dos.Record;
import cn.easyplatform.dos.UserDo;
import cn.easyplatform.entities.beans.table.TableBean;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface BizDao {

    void log(LogDo log);

    /**
     * 新增
     *
     * @param table
     * @param record
     */
    boolean insert(UserDo user, TableBean table, Record record,
                   boolean isAutoCommit);

    /**
     * 新增记录，并返回自增长id
     *
     * @param statement
     * @param parameter
     * @return
     */
    long insert(UserDo user, String statement, boolean isAutoCommit, Object... parameter);

    /**
     * 更新
     *
     * @param table
     * @param record
     */
    boolean update(UserDo user, String table, Record record,
                   boolean isAutoCommit);

    /**
     * @param statement
     * @param parameter
     */
    int update(UserDo user, String statement, List<FieldDo> parameter,
               boolean isAutoCommit);

    /**
     * @param statement
     * @param parameter
     */
    int update(UserDo user, String statement,
               boolean isAutoCommit, Object... parameter);

    /**
     * 删除
     *
     * @param table
     * @param key
     */
    boolean delete(UserDo user, String table, List<FieldDo> key,
                   boolean isAutoCommit);

    /**
     * @param statement
     * @param parameter
     * @return
     */
    List<FieldDo[]> selectList(String statement, List<FieldDo> parameter);

    /**
     * @param statement
     * @param parameter
     * @param page
     * @return
     */
    List<FieldDo[]> selectList(String statement, List<FieldDo> parameter,
                               Page page);

    /**
     * @param table
     * @param key
     * @return
     */
    Record selectByKey(TableBean table, List<FieldDo> key);

    /**
     * @param statement
     * @param parameter
     * @return
     */
    FieldDo[] selectOne(String statement, List<FieldDo> parameter);

    /**
     * 获取数据字典，以key-value双栏位查询
     *
     * @param sql
     * @param parameter
     * @return
     */
    Map<String, Object> selectDictionary(String sql, Object... parameter);

    /**
     * @param statement
     * @param parameter
     * @return
     */
    FieldDo selectObject(String statement, List<FieldDo> parameter);

    /**
     * @param statement
     * @param parameter
     * @return
     */
    int getCount(String statement, List<FieldDo> parameter);

    /**
     * @param statement
     * @return
     */
    FieldDo[] getMetaData(String statement);

    /**
     * @param statement
     * @param parameter
     * @return
     */
    ResultSet executeQuery(String statement, List<FieldDo> parameter)
            throws SQLException;

    /**
     * @param rs
     */
    void close(ResultSet rs);

    /**
     * @param statement
     * @param isAutoCommit
     */
    void prepareBatch(String statement, boolean isAutoCommit);

    /**
     * @param user
     * @param parameter
     */
    void addBatch(UserDo user, Collection<FieldDo> parameter);

    /**
     * @param user
     * @param sql
     */
    void addBatch(UserDo user, String sql);

    /**
     *
     */
    void executeBatch();


    /**
     * 选择单个栏位值
     *
     * @param statement
     * @param parameter
     * @return
     */
    Object selectObject(String statement, Object... parameter);

    /**
     * 选择单笔数据
     *
     * @param statement
     * @param parameter
     * @return
     */
    Object[] selectOne(String statement, Object... parameter);

    /**
     * 选择单笔数据
     *
     * @param statement
     * @param parameter
     * @return
     */
    Map<String, Object> selectMap(String statement, Object... parameter);

    /**
     * 选择多笔数据
     *
     * @param statement
     * @param page
     * @param parameter
     * @return
     */
    List<Object[]> selectList(String statement, Page page, Object... parameter);

    /**
     * 选择多笔数据
     *
     * @param statement
     * @param page
     * @param parameter
     * @return
     */
    List<Map<String, Object>> selectMapList(String statement, Page page, Object... parameter);

    /**
     * @param statement
     * @param parameter
     * @return
     */
    int update(String statement, UserDo user, Object... parameter);
}
