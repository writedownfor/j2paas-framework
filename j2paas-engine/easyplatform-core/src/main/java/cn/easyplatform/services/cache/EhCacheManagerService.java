/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.services.cache;

import cn.easyplatform.ServiceException;
import cn.easyplatform.i18n.I18N;
import cn.easyplatform.services.ICacheManager;
import cn.easyplatform.services.SystemServiceId;
import cn.easyplatform.spi.engine.EngineFactory;
import cn.easyplatform.spi.extension.ApplicationService;
import cn.easyplatform.type.StateType;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.config.ConfigurationException;
import org.apache.shiro.io.ResourceUtils;
import org.apache.shiro.util.Destroyable;
import org.apache.shiro.util.Initializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class EhCacheManagerService implements Initializable,
        Destroyable, ApplicationService, ICacheManager {

    private static final Logger log = LoggerFactory.getLogger(EhCacheManagerService.class);

    protected net.sf.ehcache.CacheManager manager;

    private boolean cacheManagerImplicitlyCreated = false;

    private String cacheManagerConfigFile;

    private Date startTime;

    public EhCacheManagerService() {
        StringBuilder sb = new StringBuilder(EngineFactory.me().getConfig("easyplatform.conf"));
        sb.append(File.separatorChar).append("ehcache.xml");
        cacheManagerConfigFile = sb.toString();
    }

    /**
     * @return
     */
    public net.sf.ehcache.CacheManager getCacheManager() {
        return manager;
    }

    /**
     * @param manager
     */
    public void setCacheManager(net.sf.ehcache.CacheManager manager) {
        this.manager = manager;
    }

    /**
     * @return
     */
    public String getCacheManagerConfigFile() {
        return this.cacheManagerConfigFile;
    }

    /**
     * @param classpathLocation
     */
    public void setCacheManagerConfigFile(String classpathLocation) {
        this.cacheManagerConfigFile = classpathLocation;
    }

    /**
     * @return
     */
    protected InputStream getCacheManagerConfigFileInputStream() {
        String configFile = getCacheManagerConfigFile();
        try {
            return ResourceUtils.getInputStreamForPath(configFile);
        } catch (IOException e) {
            throw new ConfigurationException(
                    "Unable to obtain input stream for cacheManagerConfigFile ["
                            + configFile + "]", e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.apache.shiro.cache.CacheManager#getCache(java.lang.String)
     */
    public final <K, V> Cache<K, V> getCache(String name) throws CacheException {

        if (log.isTraceEnabled()) {
            log.trace("Acquiring EhCache instance named [" + name + "]");
        }
        try {
            net.sf.ehcache.Ehcache cache = ensureCacheManager()
                    .getEhcache(name);
            if (cache == null) {
                if (log.isInfoEnabled()) {
                    log.info(
                            "Cache with name '{}' does not yet exist.  Creating now.",
                            name);
                }
                this.manager.addCache(name);

                cache = manager.getCache(name);

                if (log.isInfoEnabled()) {
                    log.info("Added EhCache named [" + name + "]");
                }
            }
            return new EhCache<K, V>(cache);
        } catch (net.sf.ehcache.CacheException e) {
            throw new CacheException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.apache.shiro.util.Initializable#init()
     */
    public final void init() throws CacheException {
        ensureCacheManager();
        startTime = new Date();
    }

    /**
     * @return
     */
    private net.sf.ehcache.CacheManager ensureCacheManager() {
        try {
            if (this.manager == null) {
                if (log.isDebugEnabled()) {
                    log.debug(I18N.getLabel("easyplatform.sys.common.starting",
                            getName()));
                }
                this.manager = new net.sf.ehcache.CacheManager(
                        getCacheManagerConfigFileInputStream());
                if (log.isTraceEnabled()) {
                    log.trace("instantiated Ehcache CacheManager instance.");
                }
                cacheManagerImplicitlyCreated = true;
                if (log.isDebugEnabled()) {
                    log.debug(I18N.getLabel("easyplatform.sys.common.started",
                            getName()));
                }
            }
            return this.manager;
        } catch (Exception e) {
            throw new CacheException(I18N.getLabel("easyplatform.sys.common.fail",
                    getName(), e.getMessage()));
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.apache.shiro.util.Destroyable#destroy()
     */
    public void destroy() {
        if (cacheManagerImplicitlyCreated) {
            if (log.isDebugEnabled())
                log.debug(I18N.getLabel("easyplatform.sys.common.stopping",
                        getName()));
            try {
                net.sf.ehcache.CacheManager cacheMgr = getCacheManager();
                cacheMgr.shutdown();
                if (log.isDebugEnabled())
                    log.debug(I18N.getLabel("easyplatform.sys.common.stopped",
                            getName()));
            } catch (Exception e) {
                if (log.isWarnEnabled())
                    log.warn(I18N.getLabel("easyplatform.sys.common.stop.fail",
                            getName(), e.getMessage()));
            }
            cacheManagerImplicitlyCreated = false;
        }
        startTime = null;
    }

    @Override
    public String getName() {
        return I18N.getLabel("easyplatform.sys.cache.name");
    }

    @Override
    public String getDescription() {
        return I18N.getLabel("easyplatform.sys.cache.desp");
    }

    @Override
    public void start() throws ServiceException {
    }

    @Override
    public void stop() throws ServiceException {
    }

    @Override
    public int getState() {
        return cacheManagerImplicitlyCreated ? StateType.START
                : StateType.STOP;
    }

    @Override
    public Map<String, Object> getRuntimeInfo() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(I18N.getLabel("easyplatform.sys.common.start.time"), startTime);
        return null;
    }

    @Override
    public String getId() {
        return SystemServiceId.SYS_EHCACHE;
    }

    @Override
    public boolean cacheExists(String cacheName) {
        return manager.cacheExists(cacheName);
    }

    @Override
    public void removeCache(String name) {
        manager.removeCache(name);
    }

    @Override
    public void setState(int state) {

    }

    @Override
    public Date getStartTime() {
        return startTime;
    }

    @Override
    public boolean isCluster() {
        return false;
    }


}
