/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.support.scripting.impl.js;

import org.mozilla.javascript.*;

import javax.script.Bindings;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
final class RhinoTopLevel extends ImporterTopLevel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String builtinVariables = "var com = Packages.com;                   \n"
			+ "var java = Packages.java;                   \n";

	RhinoTopLevel(Context cx) {
		super(cx);
		new LazilyLoadedCtor(this, "JSAdapter",
				"com.sun.script.javascript.JSAdapter", false);
		JavaAdapter.init(cx, this, false);
		String names[] = { "bindings", "scope", "sync" };
		defineFunctionProperties(names, RhinoTopLevel.class,
				ScriptableObject.DONTENUM);
		cx.evaluateString(this, builtinVariables, "<builtin>", 1, null);
	}
	
	/**
	 * @param cx
	 * @param thisObj
	 * @param args
	 * @param funObj
	 * @return
	 */
	public static Object bindings(Context cx, Scriptable thisObj,
			Object[] args, Function funObj) {
		if (args.length == 1) {
			Object arg = args[0];
			if (arg instanceof Wrapper) {
				arg = ((Wrapper) arg).unwrap();
			}
			if (arg instanceof ExternalScriptable) {
				Bindings bind = ((ExternalScriptable) arg).getContext();
				return Context.javaToJS(bind,
						ScriptableObject.getTopLevelScope(thisObj));
			}
		}
		return Context.getUndefinedValue();
	}

	/**
	 * @param cx
	 * @param thisObj
	 * @param args
	 * @param funObj
	 * @return
	 */
	public static Object scope(Context cx, Scriptable thisObj, Object[] args,
			Function funObj) {
		if (args.length == 1) {
			Object arg = args[0];
			if (arg instanceof Wrapper) {
				arg = ((Wrapper) arg).unwrap();
			}
			if (arg instanceof Bindings) {
				Scriptable res = new ExternalScriptable((Bindings) arg);
				res.setPrototype(ScriptableObject.getObjectPrototype(thisObj));
				res.setParentScope(ScriptableObject.getTopLevelScope(thisObj));
				return res;
			}
		}
		return Context.getUndefinedValue();
	}

	/**
	 * @param cx
	 * @param thisObj
	 * @param args
	 * @param funObj
	 * @return
	 */
	public static Object sync(Context cx, Scriptable thisObj, Object[] args,
			Function funObj) {
		if (args.length == 1 && args[0] instanceof Function) {
			return new Synchronizer((Function) args[0]);
		} else {
			throw Context.reportRuntimeError("wrong argument(s) for sync");
		}
	}
}
