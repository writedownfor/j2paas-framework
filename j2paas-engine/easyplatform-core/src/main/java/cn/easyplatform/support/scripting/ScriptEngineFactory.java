/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.support.scripting;

import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.support.scripting.impl.js.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ScriptEngineFactory {

	private ScriptEngineFactory() {
	}

	/**
	 * 新建脚本引擎
	 * 
	 * @return
	 */
	public final static ScriptEngine createEngine() {
		return new RhinoScriptEngine();
	}

	/**
	 * 新建脚本预编译引擎
	 * 
	 * @return
	 */
	public final static CompliableScriptEngine createCompilableEngine(
			ScriptCmdContext ctx, String script, RhinoScriptable scope) {
		return new RhinoCompliableScriptEngine().compile(ctx, script, scope);
	}

	/**
	 * 新建脚本预编译引擎
	 * 
	 * @return
	 */
	public final static CompliableScriptEngine createCompilableEngine(
			CommandContext cc, String script) {
		return new RhinoCompliableScriptEngine().compile(cc, script);
	}

	/**
	 * 新建表达式引擎
	 * 
	 * @return
	 */
	public final static ExpressionEngine createExpressionEngine() {
		return new RhinoExpressionEngine();
	}

	/**
	 * Mapping处理引擎
	 * 
	 * @return
	 */
	public final static MappingEngine createMappingEngine() {
		return new RhinoMappingEngine();
	}
}
