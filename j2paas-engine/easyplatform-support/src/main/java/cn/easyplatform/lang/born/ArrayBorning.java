/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.lang.born;

import cn.easyplatform.lang.Lang;

import java.lang.reflect.Array;

/**
 * 封装了生成一个数组对象的方式，它 born 的时候，需要一个参数表示数组长度
 * 
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a>
 */
public class ArrayBorning implements Borning<Object> {

	private Class<?> eleType;

	public ArrayBorning(Class<?> eleType) {
		this.eleType = eleType;
	}

	public Object born(Object... args) {
		// 第一个参数必须为整数
		if (args.length >= 1) {
			Object arg0 = args[0];
			if (arg0 instanceof Number) {
				return Array.newInstance(eleType, ((Number) arg0).intValue());
			}
		}
		throw Lang
				.makeThrow("array borning need length, arg0 should be number");
	}

}
