/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.aop;

/**
 * 类定义的代理
 * 
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a>
 */
public interface ClassAgent {

	/**
	 * 定义一个新的类对象
	 * 
	 * @param cd
	 *            字节码生成器
	 * @param klass
	 *            参照类对象
	 * @return 新的类对象
	 */
	<T> Class<T> define(ClassDefiner cd, Class<T> klass);

	/**
	 * 添加拦截器
	 * 
	 * @param matcher
	 *            方法匹配器
	 * @param inte
	 *            拦截器
	 * @return 添加完成后的ClassAgent
	 */
	ClassAgent addInterceptor(MethodMatcher matcher, MethodInterceptor inte);

	String CLASSNAME_SUFFIX = "$$AOP";
}
