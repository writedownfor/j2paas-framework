/**
 * Copyright 2019 吉鼎科技.
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex.simple;

import cn.easyplatform.EasyPlatformWithLabelKeyException;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.GetListRequestMessage;
import cn.easyplatform.messages.response.GetListResponseMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.GetListVo;
import cn.easyplatform.messages.vos.component.ListPageVo;
import cn.easyplatform.messages.vos.datalist.ListGetListVo;
import cn.easyplatform.messages.vos.datalist.ListHeaderVo;
import cn.easyplatform.messages.vos.datalist.ListVo;
import cn.easyplatform.spi.service.ComponentService;
import cn.easyplatform.type.FieldVo;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.type.ListRowVo;
import cn.easyplatform.web.exporter.excel.ExcelExporter;
import cn.easyplatform.web.exporter.pdf.PdfExporter;
import cn.easyplatform.web.ext.ComponentBuilder;
import cn.easyplatform.web.ext.Exportable;
import cn.easyplatform.web.ext.Runnable;
import cn.easyplatform.web.ext.Widget;
import cn.easyplatform.web.ext.zul.GridExt;
import cn.easyplatform.web.service.ServiceLocator;
import cn.easyplatform.web.task.zkex.ListSupport;
import cn.easyplatform.web.task.OperableHandler;
import cn.easyplatform.web.task.BackendException;
import cn.easyplatform.web.task.event.EventListenerHandler;
import cn.easyplatform.web.task.support.ExpressionEngine;
import cn.easyplatform.web.task.support.SupportFactory;
import cn.easyplatform.web.utils.ExtUtils;
import cn.easyplatform.web.utils.PageUtils;
import cn.easyplatform.web.utils.WebUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zk.ui.metainfo.EventHandler;
import org.zkoss.zk.ui.metainfo.EventHandlerMap;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkmax.zul.Filedownload;
import org.zkoss.zul.*;
import org.zkoss.zul.event.PagingEvent;
import org.zkoss.zul.event.ZulEvents;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GridBuilder implements ComponentBuilder, Widget, Exportable, Runnable, EventListener<Event> {

    private GridExt grid;

    private OperableHandler main;

    private GetListVo gv;

    private Paging paging;

    private ComponentService cs;

    private TotalHelper[] totalHelper;

    private String[] columns;

    private String eventName = "";

    private EventListenerHandler elh;

    private ListSupport support;

    private Component anchor;

    public GridBuilder(OperableHandler mainTaskHandler, GridExt cbx) {
        this.main = mainTaskHandler;
        this.grid = cbx;
    }

    public GridBuilder(ListSupport support, GridExt cbx, Component anchor) {
        this.support = support;
        this.main = support.getMainHandler();
        this.grid = cbx;
        this.anchor = anchor;
    }

    @Override
    public Component build() {
        if (grid.getQuery() == null)
            return grid;
        if (grid.isCross()) {
            if (grid.getQuery() == null)
                throw new EasyPlatformWithLabelKeyException("component.property.not.found", "<table>", "query");
            if (Strings.isBlank(grid.getRowQuery()))
                throw new EasyPlatformWithLabelKeyException("component.property.not.found", "<table>", "rowQuery");
            if (Strings.isBlank(grid.getGroupQuery()))
                throw new EasyPlatformWithLabelKeyException("component.property.not.found", "<table>", "groupQuery");
        } else if (!Strings.isBlank(grid.getGroupQuery())) {
            if (Strings.isBlank(grid.getGroupField()))
                throw new EasyPlatformWithLabelKeyException("component.property.not.found", "<table>", "groupField");
        }
        if (!Strings.isBlank(grid.getTotalColumns()) && !Strings.isBlank(grid.getTotalScript())) {
            String[] cols = grid.getTotalColumns().split(",");
            totalHelper = new TotalHelper[cols.length];
            for (int i = 0; i < cols.length; i++)
                totalHelper[i] = new TotalHelper(cols[i].toUpperCase());
        }
        grid.setAttribute("$proxy", this);
        cs = ServiceLocator.lookup(ComponentService.class);
        //表示原生的，不需要设置
        if (grid.getColumns() != null)
            grid.getColumns().setAttribute("o", 1);
        createHeads();
        if (grid.getPageSize() > 0) {
            if (!grid.isFetchAll()) {
                Vlayout layout = new Vlayout();
                layout.setSpacing("0");
                layout.setHflex(grid.getHflex());
                layout.setVflex(grid.getVflex());
                layout.setVisible(grid.isVisible());
                layout.setStyle(grid.getStyle());
                layout.setTop(grid.getTop());
                layout.setLeft(grid.getLeft());
                layout.setHeight(grid.getHeight());
                layout.setWidth(grid.getWidth());
                grid.setHeight(null);
                grid.setWidth(null);
                grid.setHflex("1");
                grid.setVflex("1");
                grid.setStyle(null);
                grid.setTop(null);
                grid.setLeft(null);
                Components.replace(grid, layout);
                grid.setParent(layout);
                paging = new Paging();
                paging.setHflex("1");
                paging.setHeight("30px");
                paging.setDetailed(grid.isPagingDetailed());
                paging.setMold(grid.getPagingMold());
                paging.setPageSize(grid.getPageSize());
                if (grid.getPagingStyle() != null)
                    paging.setStyle(grid.getPagingStyle());
                paging.addEventListener(ZulEvents.ON_PAGING, this);
                paging.setParent(layout);
            } else {
                grid.setMold("paging");
                grid.getPaginal().setPageSize(grid.getPageSize());
                paging = grid.getPagingChild();
            }
            grid.setPaging(paging);
        }
        // 设置事件
        if (!Strings.isBlank(grid.getEvent())) {
            String event = grid.getEvent();
            if (Events.isValid(event)) {
                char[] cs = event.toCharArray();
                int i = 0;
                boolean isSep = false;
                StringBuilder sb = new StringBuilder();
                while (true) {
                    if (cs[i] == ':') {
                        isSep = true;
                        break;
                    }
                    if (Character.isLetter(cs[i]))
                        sb.append(cs[i]);
                    else
                        break;
                    i++;
                }
                if (isSep && sb.length() > 0) {
                    eventName = sb.toString();
                    event = event.substring(i + 1).trim();
                }
                sb = null;
            } else
                eventName = Events.ON_DOUBLE_CLICK;
            elh = new EventListenerHandler(eventName, main, event, anchor);
        } else {
            EventHandlerMap em = grid.getEventHandlerMap();
            if (em != null && !em.isEmpty()) {
                em.getEventNames().forEach(name -> {
                    List<EventHandler> evts = em.getAll(name);
                    evts.forEach(eh -> {
                        if (eh.isEffective(grid)) {
                            if (Events.ON_CLICK.equals(name) || Events.ON_DOUBLE_CLICK.equals(name))
                                elh = new EventListenerHandler(name,
                                        main, eh.getZScript().getRawContent(), anchor);
                            else
                                grid.addEventListener(name, new EventListenerHandler(name,
                                        main, eh.getZScript().getRawContent(), anchor));

                        }
                    });
                });
            }
        }
        if (grid.isImmediate())
            createItems();
        return grid;
    }

    private void createHeads() {
        if (grid.isCross()) {
            if (grid.getFirstChild() != null)
                grid.getChildren().clear();
            ComponentService cs = ServiceLocator
                    .lookup(ComponentService.class);
            //行分组表头
            GetListVo gv = null;
            if (anchor != null) {
                ListRowVo rowVo = WebUtils.getAnchorValue(anchor);
                Object[] data = support.isCustom() ? rowVo.getData() : rowVo
                        .getKeys();
                gv = new ListGetListVo(grid.getDbId(), grid.getGroupQuery(), support
                        .getComponent().getId(), data);
            } else {
                gv = new GetListVo(grid.getDbId(), grid.getGroupQuery());
            }
            GetListRequestMessage req = new GetListRequestMessage(main.getId(), gv);
            IResponseMessage<?> resp = cs.getList(req);
            if (resp.isSuccess()) {
                List<FieldVo[]> headers = (List<FieldVo[]>) ((GetListResponseMessage) resp)
                        .getBody();
                Auxhead auxhead = new Auxhead();
                auxhead.setParent(grid);
                Auxheader auxheader = new Auxheader();
                auxheader.setColspan(1);
                auxheader.setParent(auxhead);
                StringBuilder sb = new StringBuilder(20);
                for (FieldVo[] fds : headers) {
                    for (FieldVo fv : fds)
                        sb.append(fv.getValue()).append(".");
                    auxheader = new Auxheader(sb.deleteCharAt(sb.length() - 1).toString());
                    auxheader.setAlign("center");
                    auxheader.setParent(auxhead);
                    auxheader.setAttribute("group", fds);
                    sb.setLength(0);
                }
            } else
                throw new BackendException(resp);
            req = new GetListRequestMessage(main.getId(),
                    new GetListVo(grid.getDbId(), grid.getRowQuery()));
            resp = cs.getList(req);
            if (resp.isSuccess()) {
                Columns listhead = new Columns();
                listhead.setParent(grid);
                List<FieldVo[]> headers = (List<FieldVo[]>) ((GetListResponseMessage) resp)
                        .getBody();
                if (!headers.isEmpty()) {
                    Column listheader = new Column(headers.get(0)[0].getName());
                    listheader.setAlign("center");
                    listhead.appendChild(listheader);
                }
                if (grid.getRows() == null)
                    grid.appendChild(new Rows());
                for (FieldVo[] fds : headers) {
                    Row li = new Row();
                    Label lc = new Label(fds[0].getValue().toString());
                    lc.setSclass("font-weight-bold");
                    li.appendChild(lc);
                    li.setParent(grid.getRows());
                }
            } else
                grid.setEmptyMessage((String) resp.getBody());
        } else {
            grid.setCols(0);
            if (grid.getColumns() != null) {
                if (grid.getColumns().getAttribute("o") == null) {
                    if (!Strings.isBlank(grid.getTitle()) && grid.getQuery() == null) {
                    } else {
                        grid.getColumns().detach();
                        createNormalHeader();
                    }
                }
            } else {
                createNormalHeader();
            }
        }
    }

    /**
     * 创建正常表头
     */
    private void createNormalHeader() {
        if (!Strings.isBlank(grid.getTitle())) {
            String title = grid.getTitle();
            if (title.startsWith("$")) {
                Object val = WebUtils.getFieldValue(main, title.substring(1));
                if (val == null || val.toString().equals(""))
                    return;
                title = (String) val;
            }
            Columns head = new Columns();
            head.setSizable(grid.isSizable());
            String[] headers = title.split(",");
            for (int i = 0; i < grid.getUnionRows(); i++) {
                for (String h : headers) {
                    Column header = new Column(h.trim());
                    if (!Strings.isBlank(grid.getHeadStyle()))
                        header.setStyle(grid.getHeadStyle());
                    if (grid.isSizedByContent())
                        header.setHflex("1");
                    else
                        header.setWidth(header.getLabel().length() * 15 + "px");
                    head.appendChild(header);
                }
            }
            grid.appendChild(head);
            if (grid.getFrozen() == null && grid.getFrozenColumns() > 0) {
                Frozen frozen = new Frozen();
                if (!Strings.isBlank(grid.getFrozenStyle()))
                    frozen.setStyle(grid.getFrozenStyle());
                frozen.setColumns(grid.getFrozenColumns());
                grid.appendChild(frozen);
            }
        }
        if (grid.getFoot() != null)
            grid.getFoot().detach();
    }

    /**
     * 创建交叉表记录
     *
     * @param data
     */
    private void createCrosstableItems(List<String> groupName, List<FieldVo[]> data) {
        List<Auxheader> auxheaders = grid.getFirstChild().getChildren();
        for (int i = 1; i < auxheaders.size(); i++) {
            Auxheader auxheader = auxheaders.get(i);
            FieldVo[] group = (FieldVo[]) auxheader.getAttribute("group");
            List<FieldVo[]> rows = getRows(group, data);
            for (Component li : grid.getRows().getChildren()) {
                Label lc = (Label) li.getFirstChild();
                Object value = lc.getValue();
                FieldVo[] fvs = getRow(groupName.get(0), value, rows);
                int idx = ((i - 1) * auxheader.getColspan()) + 1;
                lc = (Label) li.getChildren().get(idx);
                if (fvs != null) {//填充记录
                    for (FieldVo fv : fvs) {
                        if (!groupName.contains(fv.getName())) {
                            Component cell = li.getChildren().get(idx);
                            Component c = cell.getFirstChild();
                            if (c == null)
                                PageUtils.setValue(cell, fv.getValue());
                            else
                                PageUtils.setValue(c, fv.getValue());
                            idx++;
                        }//if
                    }//for
                    //保存当前记录到第1格子
                    //CrossRowVo crv = new CrossRowVo(fvs);
                    //lc.setValue(crv);
                } else {//创建新记录
                    fvs = data.get(0);
                    FieldVo[] clone = new FieldVo[fvs.length];
                    for (int j = 0; j < fvs.length; j++) {
                        clone[j] = fvs[j].clone();
                        if (clone[j].getName().equals(groupName.get(0))) {
                            clone[j].setValue(value);
                        } else {
                            for (FieldVo fv : group) {
                                if (clone[j].getName().equals(fv.getName())) {
                                    clone[j].setValue(fv.getValue());
                                    break;
                                }
                            }
                        }
                    }
                    //CrossRowVo crv = new CrossRowVo(clone);
                    //lc.setValue(crv);
                }//else
            }//for
        }//for
    }

    /**
     * 取单笔数据
     *
     * @param name
     * @param value
     * @param data
     * @return
     */
    private FieldVo[] getRow(String name, Object value, List<FieldVo[]> data) {
        for (FieldVo[] fvs : data) {
            for (FieldVo fv : fvs) {
                if (name.equals(fv.getName()) && Objects.equals(value, fv.getValue()))
                    return fvs;
            }
        }
        return null;
    }

    /**
     * 取组数据
     *
     * @param group
     * @param data
     * @return
     */
    private List<FieldVo[]> getRows(FieldVo[] group, List<FieldVo[]> data) {
        List<FieldVo[]> cols = new ArrayList<>();
        Iterator<FieldVo[]> itr = data.iterator();
        while (itr.hasNext()) {
            FieldVo[] fvs = itr.next();
            boolean find = false;
            for (FieldVo src : group) {
                for (FieldVo target : fvs) {
                    find = target.getName().equals(src.getName()) && Objects.equals(src.getValue(), target.getValue());
                    if (find)
                        break;
                }
                if (!find)
                    break;
            }
            if (find) {
                cols.add(fvs);
                //至少留一笔给空的记录赋值
                if (itr.hasNext())
                    itr.remove();
            }
        }
        return cols;
    }

    /**
     * 生成表格记录项
     */
    private void createItems() {
        if (grid.getRows() == null)
            grid.appendChild(new Rows());
        if (grid.getQuery() instanceof String) {
            String sql = grid.getQuery().toString();
            if (anchor != null) {
                ListRowVo rowVo = WebUtils.getAnchorValue(anchor);
                Object[] data = support.isCustom() ? rowVo.getData() : rowVo
                        .getKeys();
                gv = new ListGetListVo(grid.getDbId(), sql, support
                        .getComponent().getId(), data);
            } else {
                gv = new GetListVo(grid.getDbId(), sql);
            }
            gv.setFilter(grid.getFilter());
            gv.setPageSize(grid.isFetchAll() ? 0 : grid.getPageSize() * grid.getUnionRows());
            gv.setOrderBy(grid.getOrderBy());
            gv.setGetCount(!grid.isCross());
            gv.setGetMetadataIfEmpty(grid.isCross());
            GetListRequestMessage req = new GetListRequestMessage(main.getId(), gv);
            IResponseMessage<?> resp = cs.getList(req);
            if (!resp.isSuccess()) {
                Clients.wrongValue(grid, (String) resp.getBody());
                return;
            }
            if (grid.isCross()) {
                FieldVo[] fvs = null;
                List<FieldVo[]> data = null;
                if (resp instanceof SimpleResponseMessage) {//空的记录
                    fvs = (FieldVo[]) resp.getBody();
                    data = new ArrayList<>(1);
                    data.add(fvs);
                } else {
                    data = (List<FieldVo[]>) ((GetListResponseMessage) resp).getBody();
                    fvs = data.get(0);
                }
                Auxhead auxhead = (Auxhead) grid.getFirstChild();
                List<Auxheader> auxheaders = auxhead.getChildren();
                List<String> groupName = new ArrayList<>();
                Columns listhead = grid.getColumns();
                groupName.add(((Column) listhead.getChildren().get(0)).getLabel());
                Auxheader auxheader = auxheaders.get(1);
                FieldVo[] group = (FieldVo[]) auxheader.getAttribute("group");
                for (FieldVo f : group)
                    groupName.add(f.getName());
                for (int i = 1; i < auxheaders.size(); i++) {
                    auxheader = auxheaders.get(i);
                    auxheader.setColspan(fvs.length - groupName.size());
                    for (FieldVo fv : fvs) {
                        if (!groupName.contains(fv.getName())) {
                            Column listheader = new Column(fv.getName());
                            listheader.setParent(listhead);
                        }
                    }
                }
                //先填充，不管有没有数据
                for (Component li : grid.getRows().getChildren()) {
                    for (int i = 1; i < auxheaders.size(); i++) {
                        for (FieldVo fv : fvs) {
                            if (!groupName.contains(fv.getName())) {
                                Label lc = new Label();
                                lc.setParent(li);
                            }
                        }//for
                    }//for
                }//for
                createCrosstableItems(groupName, data);
            } else {
                grid.getRows().getChildren().clear();
                List<FieldVo[]> data = null;
                ListPageVo pv = null;
                if (resp.getBody() instanceof ListPageVo) {
                    pv = (ListPageVo) resp.getBody();
                    data = pv.getData();
                } else
                    data = (List<FieldVo[]>) ((GetListResponseMessage) resp).getBody();
                if (!data.isEmpty()) {
                    redraw(data);
                    if (pv != null) {
                        if (pv.getTotalCount() > gv.getPageSize()) {
                            paging.setTotalSize(pv.getTotalCount());
                            paging.setPageSize(gv.getPageSize());
                            paging.setVisible(true);
                        } else
                            paging.setVisible(false);
                    }
                } else if (paging != null)
                    paging.setVisible(false);
            }
        } else {
            grid.getRows().getChildren().clear();

        }
    }

    /**
     * 绘制正常表记录
     *
     * @param data
     */
    protected void redraw(List<FieldVo[]> data) {
        Map<Object, Node> map = null;
        if (!Strings.isBlank(grid.getGroupField()) && Strings.isBlank(grid.getGroupQuery()))
            map = new LinkedHashMap<Object, Node>();
        ExpressionEngine expressionEngine = null;
        Map<String, Object> evalMap = null;
        Map<String, Component> managedComponents = null;
        if (!Strings.isBlank(grid.getRowScript())) {
            expressionEngine = SupportFactory.getExpressionEngine(main, grid.getRowScript());
            expressionEngine.compile();
            evalMap = new HashMap<String, Object>();
            managedComponents = new HashMap<String, Component>();
        }
        if (totalHelper != null) {
            for (TotalHelper h : totalHelper) {
                h.total = null;
                h.total = new BigDecimal(0);
            }
        }
        try {
            boolean createTotal = true;
            int size = data.size();
            for (int i = 0; i < size; i++) {
                FieldVo[] fvs = data.get(i);
                if (map != null) {
                    for (FieldVo fv : fvs) {
                        if (fv.getName().equals(grid.getGroupField())) {
                            Node node = map.get(fv.getValue());
                            if (node == null) {
                                node = new Node();
                                node.key = fv.getValue();
                                if (!Strings.isBlank(grid.getGroupNameField())) {
                                    for (FieldVo f : fvs) {
                                        if (f.getName().equals(grid.getGroupNameField())) {
                                            node.name = f.getValue() == null ? node.key.toString()
                                                    : f.getValue().toString();
                                            break;
                                        }
                                    }
                                } else
                                    node.name = node.key.toString();
                                map.put(node.key, node);
                            }
                            node.children.add(fvs);
                            break;
                        }
                    } // for
                } else if (!Strings.isBlank(grid.getGroupQuery())) {// groupQuery
                    Group g = new Group();
                    if (!Strings.isBlank(grid.getGroupNameField())) {
                        for (FieldVo f : fvs) {
                            if (f.getName().equals(grid.getGroupNameField())) {
                                g.setLabel((String) f.getValue());
                                break;
                            }
                        }
                    } else {
                        for (FieldVo f : fvs) {
                            if (f.getName().equals(grid.getGroupField())) {
                                g.setLabel((String) f.getValue());
                                break;
                            }
                        }
                    }
                    g.setValue(fvs);
                    if (grid.isLazy()) {
                        g.addEventListener(Events.ON_OPEN, this);
                        g.setOpen(false);
                        createTotal = false;
                    }
                    grid.appendChild(g);
                    if (!grid.isLazy())
                        loadGroup(expressionEngine, evalMap, managedComponents, g);
                } else {
                    if (evalMap != null) {
                        evalMap.clear();
                        managedComponents.clear();
                    }
                    if (grid.getUnionRows() > 1) {
                        FieldVo[][] rowData = new FieldVo[grid.getUnionRows()][fvs.length];
                        for (int j = 0; j < rowData.length; j++) {
                            if ((i + j) < size)
                                rowData[j] = data.get(i + j);
                        }
                        createUnionRow(grid.getRows().getChildren().size(), rowData, evalMap, managedComponents, expressionEngine);
                        i += grid.getUnionRows() - 1;
                    } else {
                        Row anchor = createRow(grid.getRows().getChildren().size(), fvs, evalMap, managedComponents, null);
                        if (expressionEngine != null)
                            expressionEngine.eval(anchor, evalMap, managedComponents);
                    }
                }
            }
            if (map != null) {
                for (Node node : map.values()) {
                    Listgroup g = new Listgroup();
                    g.setLabel(node.name);
                    TotalHelper[] groupTotals = null;
                    if (totalHelper != null) {
                        groupTotals = new TotalHelper[totalHelper.length];
                        for (int i = 0; i < totalHelper.length; i++)
                            groupTotals[i] = new TotalHelper(totalHelper[i].name);
                    }
                    grid.appendChild(g);
                    for (FieldVo[] fvs : node.children) {
                        if (evalMap != null) {
                            evalMap.clear();
                            managedComponents.clear();
                        }
                        Row anchor = createRow(grid.getRows().getChildren().size(), fvs, evalMap, managedComponents, groupTotals);
                        if (expressionEngine != null)
                            expressionEngine.eval(anchor, evalMap, managedComponents);
                    }
                    if (groupTotals != null)
                        createFoot(g, groupTotals);
                }
            }
            if (createTotal && totalHelper != null)
                createFoot(grid, totalHelper);
        } finally {
            if (expressionEngine != null) {
                evalMap = null;
                managedComponents = null;
                expressionEngine.destroy();
                expressionEngine = null;
            }
        }
    }

    private void createFoot(Component parent, TotalHelper[] data) {
        ExpressionEngine footEngine = SupportFactory.getExpressionEngine(main, grid.getTotalScript());
        Map<String, Component> managedComponents = new HashMap<String, Component>();
        // 创建新的统计行
        if (parent instanceof GridExt) {
            if (((GridExt) parent).getFoot() == null) {
                Foot foot = new Foot();
                for (int i = 0; i < columns.length; i++) {
                    Footer footer = new Footer();
                    footer.setParent(foot);
                    managedComponents.put(columns[i], footer);
                }
                foot.setParent(parent);
            } else {
                List<Component> footers = (((GridExt) parent).getFoot()).getChildren();
                for (int i = 0; i < columns.length; i++)
                    managedComponents.put(columns[i], footers.get(i));
            }
        } else {
            managedComponents.put(columns[0], parent.getFirstChild());
            for (int i = 1; i < columns.length; i++) {
                Listcell footer = new Listcell();
                footer.setParent(parent);
                managedComponents.put(columns[i], footer);
            }
        }
        Map<String, Object> evalMap = new HashMap<String, Object>(data.length);
        for (TotalHelper h : data)
            evalMap.put(h.name, h.total.doubleValue());
        if (parent instanceof Listbox) {
            evalMap.put("_TYPE_", "FOOT");
            evalMap.put("_COUNT_", grid.getRows().getChildren().size());
        } else {
            evalMap.put("_TYPE_", "GROUP");
            evalMap.put("_COUNT_", ((Listgroup) parent).getItemCount());
        }
        footEngine.exec(null, managedComponents, evalMap);
        footEngine = null;
        managedComponents = null;
        evalMap = null;
    }

    /**
     * 生成多笔记录的表行
     *
     * @param index
     * @param data
     * @param evalMap
     * @param managedComponents
     * @param expressionEngine
     */
    private void createUnionRow(int index, FieldVo[][] data, Map<String, Object> evalMap,
                                Map<String, Component> managedComponents, ExpressionEngine expressionEngine) {
        Row item = new Row();
        if (!Strings.isBlank(grid.getRowStyle()))
            item.setStyle(grid.getRowStyle());
        item.setValue(data[0]);
        if (columns == null)
            columns = new String[data[0].length * grid.getUnionRows()];
        int pos = 0;
        for (int i = 0; i < data.length; i++) {
            FieldVo[] fvs = data[i];
            if (fvs[0] == null)
                item.appendChild(new Label());
            else {
                if (evalMap != null) {
                    for (FieldVo fv : fvs)
                        evalMap.put(fv.getName(), fv.getValue());
                }
                for (int j = 0; j < fvs.length; j++, pos++) {
                    FieldVo field = fvs[j];
                    columns[pos] = field.getName();
                    Label cell = new Label();
                    if (field.getValue() != null)
                        cell.setValue(WebUtils.format(field));
                    if (managedComponents != null)
                        managedComponents.put(field.getName(), cell);
                    item.appendChild(cell);
                }
                if (expressionEngine != null)
                    expressionEngine.eval(item, evalMap, managedComponents);
            }
        }
        if (!Strings.isBlank(grid.getDraggable()))
            item.setDraggable(grid.getDraggable());
        if (elh != null)
            item.addEventListener(eventName, this);
        grid.getRows().getChildren().add(index, item);
    }

    private Row createRow(int index, FieldVo[] fvs, Map<String, Object> evalMap,
                          Map<String, Component> managedComponents, TotalHelper[] groupTotals) {
        Row item = new Row();
        if (!Strings.isBlank(grid.getRowStyle()))
            item.setStyle(grid.getRowStyle());
        item.setValue(fvs);
        int size = grid.getCols();
        if (size == 0) {
            size = fvs.length;
            if (grid.getColumns() != null && !grid.getColumns().getChildren().isEmpty())
                size = grid.getColumns().getChildren().size();
            grid.setCols(size);
            columns = new String[size];
        }
        if (evalMap != null) {
            for (FieldVo fv : fvs)
                evalMap.put(fv.getName(), fv.getValue());
        }
        for (int i = 0; i < fvs.length; i++) {
            FieldVo field = fvs[i];
            if (totalHelper != null) {
                for (int j = 0; j < totalHelper.length; j++) {
                    if (totalHelper[j].name.equals(field.getName()) && field.getValue() != null) {
                        totalHelper[j].caculate(field.getValue().toString());
                        if (groupTotals != null)
                            groupTotals[j].caculate(field.getValue().toString());
                        break;
                    }
                }
            }
            if (i < columns.length) {
                columns[i] = field.getName();
                Label cell = new Label();
                if (field.getValue() != null)
                    cell.setValue(WebUtils.format(field));
                if (managedComponents != null)
                    managedComponents.put(field.getName(), cell);
                item.appendChild(cell);

            } else
                break;
        }
        if (!Strings.isBlank(grid.getDraggable()))
            item.setDraggable(grid.getDraggable());
        if (elh != null)
            item.addEventListener(eventName, this);
        grid.getRows().getChildren().add(index, item);
        return item;
    }

    private void loadGroup(ExpressionEngine expressionEngine, Map<String, Object> evalMap,
                           Map<String, Component> managedComponents, Group group) {
        GetListVo vo = new GetListVo(grid.getDbId(), grid.getGroupQuery());
        GetListRequestMessage req = new GetListRequestMessage(main.getId(), vo);
        FieldVo[] fvs = group.getValue();
        for (FieldVo f : fvs) {
            if (f.getName().equals(grid.getGroupField())) {
                vo.setParameter(f.getValue());
                break;
            }
        }
        IResponseMessage<?> resp = cs.getList(req);
        if (resp.isSuccess()) {
            TotalHelper[] groupTotals = null;
            if (totalHelper != null) {
                groupTotals = new TotalHelper[totalHelper.length];
                for (int i = 0; i < totalHelper.length; i++)
                    groupTotals[i] = new TotalHelper(totalHelper[i].name);
            }
            List<FieldVo[]> tmp = (List<FieldVo[]>) ((GetListResponseMessage) resp).getBody();
            int index = group.getIndex() + 1;
            for (FieldVo[] row : tmp) {
                if (evalMap != null) {
                    evalMap.clear();
                    managedComponents.clear();
                }
                Row item = createRow(index++, row, evalMap, managedComponents, groupTotals);
                if (expressionEngine != null)
                    expressionEngine.eval(item, evalMap, managedComponents);
            }
            if (groupTotals != null)
                createFoot(group, groupTotals);

        } else
            Clients.wrongValue(grid, (String) resp.getBody());
        group.removeEventListener(Events.ON_OPEN, this);
    }

    @Override
    public void onEvent(Event evt) throws Exception {
        if (evt.getName().equals(ZulEvents.ON_PAGING)) {
            int pageNo = ((PagingEvent) evt).getActivePage() + 1;
            gv.setGetCount(false);
            gv.setPageNo(pageNo);
            GetListRequestMessage req = new GetListRequestMessage(main.getId(), gv);
            IResponseMessage<?> resp = cs.getList(req);
            if (resp.isSuccess()) {
                grid.getRows().getChildren().clear();
                List<FieldVo[]> data = (List<FieldVo[]>) ((GetListResponseMessage) resp).getBody();
                if (!data.isEmpty())
                    redraw(data);
            } else
                throw new WrongValueException(grid, (String) resp.getBody());
        } else if (evt.getName().equals(Events.ON_OPEN)) {
            OpenEvent oe = (OpenEvent) evt;
            if (oe.isOpen()) {
                ExpressionEngine expressionEngine = null;
                Map<String, Object> evalMap = null;
                Map<String, Component> managedComponents = null;
                if (!Strings.isBlank(grid.getRowScript())) {
                    expressionEngine = SupportFactory.getExpressionEngine(main, grid.getRowScript());
                    expressionEngine.compile();
                    evalMap = new HashMap<String, Object>();
                    managedComponents = new HashMap<String, Component>();
                }
                try {
                    loadGroup(expressionEngine, evalMap, managedComponents, (Group) evt.getTarget());
                } finally {
                    if (expressionEngine != null) {
                        evalMap = null;
                        managedComponents = null;
                        expressionEngine.destroy();
                        expressionEngine = null;
                    }
                }
                // 更新总计
                if (totalHelper != null)
                    createFoot(grid, totalHelper);
            }
        } else if (evt.getName().equals(eventName))
            elh.onEvent(new Event(eventName, grid, evt.getData()));
    }

    @Override
    public void reload(Component widget) {
        createHeads();
        createItems();
    }

    @Override
    public void export(String type, Object... exportHeaders) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            ListVo entity = new ListVo(grid.getId());
            List<Component> head = grid.getColumns().getChildren();
            int size = head.size();
            List<ListHeaderVo> headers = new ArrayList<ListHeaderVo>(size);
            for (int i = 0; i < size; i++) {
                ListHeaderVo headerVo = new ListHeaderVo();
                headerVo.setName(((Column) head.get(i)).getLabel());
                if (exportHeaders.length == 0)
                    headerVo.setExport(true);
                else {
                    if (exportHeaders[0] instanceof Number) {
                        for (Object idx : exportHeaders) {
                            if (((Number) idx).intValue() == i) {
                                headerVo.setExport(true);
                                break;
                            }
                        }
                    } else if (exportHeaders[0] instanceof Strings) {
                        for (Object name : exportHeaders) {
                            if (name.equals(headerVo.getName())) {
                                headerVo.setExport(true);
                                break;
                            }
                        }
                    }
                }
                headers.add(headerVo);
            }
            entity.setHeaders(headers);
            if (type.equalsIgnoreCase("pdf")) {
                PdfExporter exporter = new PdfExporter();
                exporter.export(entity, grid, out);
                AMedia amedia = new AMedia(grid.getId() + ".pdf", "pdf", "application/pdf", out.toByteArray());
                Filedownload.save(amedia);
            } else {
                ExcelExporter exporter = new ExcelExporter();
                exporter.export(entity, grid, out);
                AMedia amedia = new AMedia(grid.getId() + ".xlsx", "xlsx", "application/file", out.toByteArray());
                Filedownload.save(amedia);
            }
        } catch (Exception ex) {
            if (log.isErrorEnabled())
                log.error("export {},{}", grid.getId(), ex);
            throw new RuntimeException(ex);
        } finally {
            IOUtils.closeQuietly(out);
        }
    }

    private final static Logger log = LoggerFactory.getLogger(GridBuilder.class);

    private class Node {
        Object key;
        String name;
        List<FieldVo[]> children = new ArrayList<FieldVo[]>();
    }

    private class TotalHelper {
        String name;
        BigDecimal total = new BigDecimal(0);

        TotalHelper(String name) {
            this.name = name;
        }

        void caculate(String amount) {
            BigDecimal val = new BigDecimal(amount);
            total = total.add(val);
        }
    }

    /**
     * Run Task
     *
     * @param args
     */
    @Override
    public void go(Object[] args) {
        ExtUtils.go(main, grid, args);
    }
}
