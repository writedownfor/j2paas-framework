/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task;

import cn.easyplatform.messages.vos.NextVo;
import cn.easyplatform.messages.vos.datalist.ListBatchVo;
import cn.easyplatform.web.task.event.EventEntry;
import cn.easyplatform.web.task.support.ManagedComponent;
import org.zkoss.zk.ui.Component;

import java.util.Collection;
import java.util.Map;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface MainTaskSupport extends OperableHandler {

    /**
     * 返回流程的上一个task
     *
     * @param actionFlag
     */
    void prev(int actionFlag);

    /**
     * 确认流程的下一个task
     *
     * @param entry
     */
    void next(EventEntry<NextVo> entry);

    /**
     * 关闭功能
     *
     * @param normal
     */
    void close(boolean normal);

    /**
     * 添加子功能
     *
     * @param id
     * @param child
     */
    void appendChild(String id, MainTaskSupport child);

    /**
     * 获取所有子功能
     *
     * @return
     */
    Map<String, MainTaskSupport> getChildren();

    /**
     * 获取受管的实体对象
     *
     * @return
     */
    Collection<ManagedComponent> getManagedEntityComponents();

    /**
     * 获取受管的绑定栏位
     *
     * @return
     */
    Collection<Component> getManagedInputComponents();

    /**
     * 根据id获取受管的实体对象
     *
     * @param id
     * @return
     */
    ManagedComponent getManagedEntityComponent(String id);

    /**
     * 获取页面源代码
     *
     * @return
     */
    String getSourceCode();

    /**
     * 当前页面的定义脚本
     *
     * @return
     */
    String getScript();

    /**
     * 是否显示
     *
     * @return
     */
    boolean isVisible();

    /**
     * 列表批量修改，选中多笔记录后由后继页面处理第一笔，处理完后继续第二笔，直到处理完后之再回到第一个页面
     *
     * @param entry
     */
    void batch(ListBatchVo entry);

    /**
     * 在当前功能设置有效的变量
     *
     * @param name
     * @param value
     */
    void set(String name, Object value);

    /**
     * 根据名称获取变量值
     *
     * @param name
     * @return
     */
    Object get(String name);

    /**
     * 移除指定的变量
     *
     * @param name
     * @return
     */
    Object remove(String name);

    /**
     * 订阅消息
     *
     * @param topic
     */
    void subscribe(String topic);

    /**
     * 取消订阅
     */
    void unsubscribe();
}
