/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex.simple;

import cn.easyplatform.EasyPlatformWithLabelKeyException;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.type.FieldVo;
import cn.easyplatform.web.ext.Widget;
import cn.easyplatform.web.ext.zul.StepbarExt;
import cn.easyplatform.web.task.OperableHandler;
import cn.easyplatform.web.task.zkex.ListSupport;
import cn.easyplatform.web.utils.PageUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zkmax.zul.Step;

import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class StepbarBuilder extends AbstractQueryBuilder<StepbarExt> implements Widget {


    public StepbarBuilder(OperableHandler mainTaskHandler, StepbarExt comp) {
        super(mainTaskHandler, comp);
    }

    public StepbarBuilder(ListSupport support, StepbarExt comp, Component anchor) {
        super(support, comp, anchor);
    }

    @Override
    public Component build() {
        PageUtils.checkAccess(main.getAccess(), me);
        me.setAttribute("$proxy", this);
        if (me.isImmediate())
            load();
        PageUtils.processEventHandler(main, me, anchor);
        return me;
    }

    @Override
    public void reload(Component widget) {
        me.getChildren().clear();
        load();
    }

    @Override
    protected void createModel(List<?> data) {
        for (Object fv : data) {
            Object[] fvs = (Object[]) fv;
            boolean isRaw = !(fvs[0] instanceof FieldVo);
            Step step = new Step();
            step.setAttribute("value", isRaw ? fvs[0] : ((FieldVo) fvs[0]).getValue());
            if (fvs.length == 1) {
                Object val = isRaw ? fvs[0] : ((FieldVo) fvs[0]).getValue();
                step.setTitle(val == null ? "" : val.toString());
            } else if (fvs.length == 2) {
                Object val = isRaw ? fvs[1] : ((FieldVo) fvs[1]).getValue();
                step.setTitle(val == null ? "" : val.toString());
            } else if (fvs.length == 3) {
                Object val = isRaw ? fvs[1] : ((FieldVo) fvs[1]).getValue();
                step.setTitle(val == null ? "" : val.toString());
                val = isRaw ? fvs[2] : ((FieldVo) fvs[2]).getValue();
                step.setIconSclass((String) val);
            }
            me.appendChild(step);
        }
    }
}
