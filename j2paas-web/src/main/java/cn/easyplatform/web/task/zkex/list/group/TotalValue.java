/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex.list.group;

import cn.easyplatform.lang.Strings;

import java.math.BigDecimal;

import static cn.easyplatform.type.TotalType.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
class TotalValue {

    private int index;

    private int totalType;

    private int count;

    private BigDecimal value;

    private BigDecimal total;

    /**
     * @param field
     */
    TotalValue(int totalType, int index) {
        this.totalType = totalType;
        this.index = index;
    }

    public int getTotalType() {
        return totalType;
    }

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * @return the value
     */
    public String getValue(String format) {
        if (Strings.isBlank(format))
            return String.valueOf(getValue());
        else
            return Strings.format(format, getValue());
    }

    /**
     * @param value the value to set
     */
    public void caculate(String amount) {
        if (total == null)
            reset();
        count++;
        BigDecimal val = new BigDecimal(amount);
        total = total.add(val);
        if (totalType == MAX) {
            if (this.value.compareTo(val) < 0)
                this.value = val;
        } else if (totalType == MIN) {
            if (this.value.compareTo(val) > 0)
                this.value = val;
        }
    }

    /**
     *
     */
    void reset() {
        total = new BigDecimal(0);
        value = new BigDecimal(0);
        count = 0;
    }

    /**
     * @return
     */
    int getCount() {
        return count;
    }

    /**
     * @return
     */
    double getTotal() {
        return this.total.doubleValue();
    }

    /**
     * @return the value
     */
    public double getValue() {
        BigDecimal val = null;
        if (totalType == AVERAGE) {
            if (count == 0)
                val = new BigDecimal(0);
            else {
                val = this.total.divide(new BigDecimal(count), 10,
                        BigDecimal.ROUND_HALF_DOWN);
            }
        } else if (totalType == COUNT)
            val = new BigDecimal(count);
        else if (totalType == MAX || totalType == MIN)
            val = this.value;
        else
            val = this.total;
        if (val == null)
            return 0;
        return val.doubleValue();
    }

    /**
     * @param value the value to set
     */
    public void setValue(double value) {
        this.value = new BigDecimal(value);
        this.total = new BigDecimal(value);
    }

}
