/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex.list.exporter;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.vos.datalist.ListHeaderVo;
import cn.easyplatform.web.ext.EntityExt;
import cn.easyplatform.web.task.zkex.ListSupport;
import cn.easyplatform.web.task.OperableHandler;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;

import java.util.ArrayList;
import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class AbstractExporter implements Exporter,
        EventListener<Event> {

    protected OperableHandler main;

    protected ListSupport support;

    private Radiogroup rg;//导出类型

    private Radiogroup rrg;//导出范围

    private Listbox listbox;

    AbstractExporter(OperableHandler main, ListSupport support) {
        this.main = main;
        this.support = support;
    }

    @Override
    public void doExport() {
        Window win = new Window();
        win.setPage(main.getComponent().getPage());
        win.setTitle(Labels.getLabel("datalist.export.title"));
        win.setClosable(true);
        win.setSizable(true);
        win.setBorder(true);
        win.setPosition("center");
        win.setShadow(true);
        createContent(win);
        win.doHighlighted();
    }

    private void createContent(Component win) {
        listbox = new Listbox();
        Listhead head = new Listhead();
        head.setSizable(true);
        Listheader header = new Listheader(
                Labels.getLabel("datalist.export.list"));
        header.setWidth("298px");
        head.appendChild(header);
        listbox.appendChild(head);
        listbox.setCheckmark(true);
        listbox.setMultiple(true);
        listbox.setAttribute("org.zkoss.zul.listbox.rod", false);
        listbox.setModel(createHeadersModel());
        listbox.setItemRenderer(new ListitemRenderer<ListHeaderVo>() {
            @Override
            public void render(Listitem item, ListHeaderVo hv, int arg2)
                    throws Exception {
                if (Strings.isBlank(hv.getTitle())) {
                    item.setVisible(false);
                    hv.setExport(false);
                } else {
                    item.setLabel(hv.getTitle());
                    item.setSelected(true);
                }
                item.setValue(hv);
            }
        });
        listbox.setVflex("1");
        listbox.setWidth("300px");
        Borderlayout layout = new Borderlayout();
        layout.setHeight("500px");
        layout.setWidth("550px");
        West west = new West();
        west.setParent(layout);
        west.setHflex("1");
        west.setVflex("1");
        west.setBorder("none");
        west.appendChild(listbox);

        Center center = new Center();
        center.setHflex("1");
        center.setVflex("1");
        center.setBorder("none");

        Vlayout vl = new Vlayout();
        vl.setSpacing("20px");
        vl.setHflex("1");
        vl.setVflex("1");
        vl.setStyle("padding:5px");
        Groupbox gb = new Groupbox();
        gb.setClosable(false);
        gb.setHflex("1");
        gb.setVflex("1");
        gb.setTitle(Labels.getLabel("datalist.export.type") + ":");
        rg = new Radiogroup();
        rg.setHflex("1");
        rg.setVflex("1");
        rg.setParent(gb);
        Hlayout hl = new Hlayout();
        Radio r = new Radio();
        r.setValue("pdf");
        r.setLabel("Pdf");
        r.setHflex("1");
        r.setVflex("1");
        r.setIconSclass("z-icon-file-pdf-o");
        r.setParent(hl);
        r = new Radio();
        r.setHflex("1");
        r.setVflex("1");
        r.setSelected(true);
        r.setLabel("Excel");
        r.setIconSclass("z-icon-file-excel-o");
        r.setValue("xls");
        r.setParent(hl);
        hl.setParent(rg);
        gb.setParent(vl);

        gb = new Groupbox();
        gb.setClosable(false);
        gb.setHflex("1");
        gb.setVflex("1");
        gb.setTitle(Labels.getLabel("datalist.export.content") + ":");
        gb.setParent(vl);
        hl = new Hlayout();
        hl.setSpacing("35px");
        rrg = new Radiogroup();
        r = new Radio(Labels.getLabel("datalist.export.head"));
        r.setParent(hl);
        r = new Radio(Labels.getLabel("datalist.export.foot"));
        r.setParent(hl);
        hl.setParent(rrg);
        rrg.setSelectedIndex(0);
        rrg.setParent(gb);

        Hlayout hb = new Hlayout();
        hb.setHflex("1");
        hb.setSclass("text-right");
        hb.setSpacing("5px");
        Button cancel = new Button(Labels.getLabel("button.cancel"));
        cancel.setIconSclass("z-icon-times");
        cancel.addForward(Events.ON_CLICK, win, Events.ON_CLOSE);
        cancel.setParent(hb);
        Button ok = new Button(Labels.getLabel("button.export"));
        ok.setIconSclass("z-icon-download");
        ok.addEventListener(Events.ON_CLICK, this);
        ok.setParent(hb);

        hb.setParent(vl);

        vl.setParent(center);
        center.setParent(layout);
        win.appendChild(layout);
    }

    protected abstract ListModel<?> createHeadersModel();

    @SuppressWarnings("unchecked")
    @Override
    public void onEvent(Event arg0) throws Exception {
        List<Integer> indexes = new ArrayList<>();
        int size = listbox.getItemCount();
        for (int i = 0; i < size; i++) {
            Listitem item = listbox.getItemAtIndex(i);
            if (item.isSelected())
                indexes.add(i);
        }
        Integer[] args = new Integer[indexes.size()];
        if (args.length == 0)
            return;
        indexes.toArray(args);
        if (rg.getSelectedIndex() == 0) {
            if (rrg.getSelectedIndex() == 0)
                support.export("pdf", args);
            else
                support.exportAll("pdf", args);
        } else {
            if (rrg.getSelectedIndex() == 0)
                support.export("xlsx", args);
            else
                support.exportAll("xlsx", args);
        }
        rg.getRoot().detach();
    }

    public static final <T extends EntityExt> Exporter createExporter(
            OperableHandler main, ListSupport target) {
        if (target.getComponent() instanceof Listbox)
            return new ListboxExportImpl(main, target);
        else if (target.getComponent() instanceof Grid)
            return new GridExportImpl(main, target);
        else
            return new TreeExportImpl(main, target);
    }
}
