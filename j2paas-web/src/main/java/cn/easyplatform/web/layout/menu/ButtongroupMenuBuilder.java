/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.layout.menu;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.vos.AuthorizationVo;
import cn.easyplatform.web.utils.ExtUtils;
import cn.easyplatform.web.utils.PageUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Button;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ButtongroupMenuBuilder extends AbstractMenuBuilder<Component> {

    private Component epcontainer;

    private List<Component> children;

    public ButtongroupMenuBuilder(Component buttongroup) {
        super(buttongroup);
    }

    @Override
    public void fill(AuthorizationVo av) {
        epcontainer = box.getFellowIfAny("epcontainer");
        if (epcontainer == null)
            return;
        children = new ArrayList<>(epcontainer.getChildren());
        for (Component c : box.getChildren()) {
            if(c instanceof Button) {
                ((Button) c).setZclass("btn");
                c.addEventListener(Events.ON_CLICK, this);
            }
        }
        String theme = (String) box.getAttribute("theme");
        if (theme == null)
            theme = "btn-default";
        String active = (String) box.getAttribute("active");
        if (active == null)
            active = "btn-info";
        box.setClientAttribute("theme", theme);
        box.setClientAttribute("active", active);
        box.setWidgetOverride("bind_","function(){this.$supers('bind_',arguments);var $s=jq(this);var t=$s.attr('theme');var s=$s.attr('active');$s.children().addClass(t);$s.children().click(function(){jq(this).removeClass(t).addClass(s).siblings().removeClass(s).addClass(t)})}");
        super.fill(av);
    }

    protected void createMenu(Menu menu) {
        Button button = new Button();
        button.setLabel(menu.getMenu().getName());
        if (!Strings.isBlank(menu.getMenu().getImage()))
            PageUtils.setTaskIcon(button, menu.getMenu().getImage());
        else
            button.setIconSclass(ExtUtils.getIconSclass());
        button.setZclass("btn");
        button.setAttribute("menu", menu);
        button.addEventListener(Events.ON_CLICK, this);
        button.setParent(box);
        children.add(new Borderlayout());
    }

    @Override
    public void onEvent(Event event) throws Exception {
        Button button = (Button) event.getTarget();
        int idx = box.getChildren().indexOf(button);
        if (epcontainer.getFirstChild() != null) {
            if (children.indexOf(epcontainer.getFirstChild()) == idx)
                return;
            epcontainer.getFirstChild().detach();
        }
        Component layout = children.get(idx);
        if (layout.getFirstChild() == null) {
            Menu mv = (Menu) button.getAttribute("menu");
            createPanel(layout, mv);
        }
        epcontainer.appendChild(layout);
    }
}
