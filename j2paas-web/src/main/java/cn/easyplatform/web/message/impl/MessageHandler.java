/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.message.impl;

import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.vos.SqlVo;
import cn.easyplatform.messages.vos.h5.MessageVo;
import cn.easyplatform.spi.service.AddonService;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.web.contexts.Contexts;
import cn.easyplatform.web.service.ServiceLocator;
import cn.easyplatform.web.utils.ExtUtils;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Label;

import javax.servlet.http.HttpServletRequest;

import static cn.easyplatform.messages.vos.h5.MessageVo.TYPE_NOTICE;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MessageHandler implements EventListener<Event> {

    private String type;

    private Label badge;

    private int unreadCount = 0;

    private boolean isMobile;

    public MessageHandler(String type, Component component) {
        this.type = type;
        this.badge = (Label) component.query("label");
        isMobile = ExtUtils.getDeviceType((HttpServletRequest) Executions.getCurrent().getNativeRequest()).equals("mil");
        AddonService ms = ServiceLocator.lookup(AddonService.class);
        SqlVo vo = new SqlVo();
        vo.setQuery("select count(*) from sys_notice_detail_info b,sys_notice_info a where a.type=? and b.touser=? and b.status=? and a.id=b.msgid");
        vo.setParameter(type);
        vo.setParameter(Contexts.getUser().getId());
        vo.setParameter(MessageVo.STATE_UNREAD);
        IResponseMessage<?> resp = ms.selectObject(new SimpleRequestMessage(vo));
        if (resp.isSuccess() && resp.getBody() != null) {
            unreadCount = ((Number) resp.getBody()).intValue();
            if (unreadCount > 0 && !isMobile)
                Clients.showNotification(Labels.getLabel("msn.message.notity",
                        new Object[]{unreadCount}), "info", badge,
                        "after_center", 10000, false);
            badge.setValue(String.valueOf(unreadCount));
        } else
            badge.setValue("0");
        Contexts.subscribe(type, this);
    }

    @Override
    public void onEvent(Event event) throws Exception {
        Integer val = 1;
        if (event.getData() instanceof Integer)
            val = (Integer) event.getData();
        unreadCount += val;
        if (unreadCount < 0)
            unreadCount = 0;
        badge.setValue(String.valueOf(unreadCount));
        if (!isMobile) {
            String msg = null;
            if (type.equals(TYPE_NOTICE))
                msg = Labels.getLabel("msn.message.notity",
                        new Object[]{unreadCount});
            else
                msg = Labels.getLabel("msn.custom.notity", new Object[]{unreadCount});
            Clients.showNotification(msg, "info", badge, "after_center", 10000, false);
        }
    }

    /**
     * 消息数减1
     */
    public void reduce() {
        unreadCount--;
        if (unreadCount < 0)
            unreadCount = 0;
        badge.setValue(String.valueOf(unreadCount));
    }
}
